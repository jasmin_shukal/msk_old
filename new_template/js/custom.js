$(document).on("scroll", function () {
    if ($(document).scrollTop() > 86) {
        $(".vfe-nav").addClass("shrink");
    } else {
        $(".vfe-nav").removeClass("shrink");
    }
});
/********************Read more and less**************************/

var showChar = 580;
if (window.outerWidth < 768) 
    var showChar = 0;

var ellipsestext = " ";
var moretext = "Read More...<i class='fa fa-angle-right' aria-hidden='true'></i>";
var lesstext = "<i class='fa fa-angle-left' aria-hidden='true'></i> Less";
$('.about-profile-text').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
        var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span><span class="remaining-content"><span>' + hide_content + '</span> <button  type="button" class="morelink">' + moretext + '</button></span>';
        $(this).html(html);
    }
});
/******************/
$(document).ready(function () {
    if (window.outerWidth < 768) {
        $(".address-box-full-address").css('display', 'none');
        $(".city-name-click1").click(function () {
            $(".city-add-open1").slideToggle("slow");           
        });  
        $(".city-name-click2").click(function () {
            $(".city-add-open2").slideToggle("slow");
        });
        $(".city-name-click3").click(function () {
            $(".city-add-open3").slideToggle("slow");
        });
        $(".city-name-click4").click(function () {
            $(".city-add-open4").slideToggle("slow");
        });
    }     
});

/******************/


$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});


$(document).ready(function () {
    $(".navbar-nav .nav-item").removeClass("active");
    var path = window.location.pathname;
    var startIndex = path.indexOf("/");
    var lastIndex = path.lastIndexOf("/");
    var page;
    if (startIndex == lastIndex) page = path.substring(path.indexOf("/") + 1);
    else page = path.substring(path.indexOf("/") + 1, path.lastIndexOf("/"));
  //  console.log(page);
    if (page.toUpperCase() == "HOME") {
        page = path.substring(path.lastIndexOf("/") + 1);
        if (page.toUpperCase() == "INDEX") page = "HOME";
        else if (page.toLowerCase() == "messagefromthefounder") page = "HOME";
    }
    if (page == "" || page == undefined) page = "HOME";
    $(".navbar-nav .nav-item").removeClass("active");
    $("#" + page.toLowerCase()).addClass("active");
});
/*******toggleClass********/
$(document).ready(function () {
    $(".navbar-toggler").click(function () {
        $(this).toggleClass('crossMenu');
    });        
});
/**************Mobile Custom Read more***************/
$(document).ready(function () {
    $(".mobile-readmore").click(function () {
        $(".mobile-collapse").slideToggle("slow");
        $(this).hide();
        $(".mobile-collapse").append("<button href='#' class='mobile-lessmore'>Less...</button>");
    });
});
$(document).on("click", ".mobile-lessmore", function () {
    $(".mobile-collapse").slideToggle("slow");
    $(this).css('display', 'none');
    $(".mobile-readmore").show();
});
$(document).ready(function () {
    $(".mobile-readmore2").click(function () {
        $(".mobile-collapse2").slideToggle("slow");
        $(this).css('display', 'none');
        $(".mobile-collapse2").append("<button href='#' class='mobile-lessmore2'>Less...</button>");
    });
});
$(document).on("click", ".mobile-lessmore2", function () {
    $(".mobile-collapse2").slideToggle("slow");
    $(this).css('display', 'none');
    $(".mobile-readmore2").show();
});

/*Image Carousel*/
const navLeft = document.querySelector(".left");
const navRight = document.querySelector(".right");

const images = document.querySelector(".images");
const colors = document.querySelector(".colored-backgrounds");

let index = 0;

function right() {
  transform((index = index < 3 ? ++index : 0));
}

function left() {
  transform((index = index > 0 ? --index : 3));
}

navLeft.addEventListener("click", left);
navRight.addEventListener("click", right);

function transform(index) {
  images.style.transform = `translateX(-${index * 100}%)`;
  colors.style.transform = `translateX(-${index * 100}%)`;
}

/*end of image carousel*/