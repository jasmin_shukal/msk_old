﻿
    customizeSelectFields();

    $(".habout").click(function () {
        hearAboutUsChange();
    });

    $("#userDetail_HearAboutUs").change(function () {
        hearAboutUsChange();
    }).trigger('change');

    function hearAboutUsChange() {
        var selectedOption = $("#userDetail_HearAboutUs option:selected").val();
        // console.log(selectedOption);

        if (selectedOption == "Family or Friends" || selectedOption == "Exclusive Offers" || selectedOption == "" || selectedOption == undefined) {
            $("#other_hear_about_us").css('display', 'none');
            $("#otherHearAboutUs").val("");
        }
        else {
            $("#other_hear_about_us").css('display', 'block');
        }
    }

    $(".hel").click(function () {
        educationChange();
    });

    $("#userDetail_EducationId").change(function () {
        educationChange();
    }).trigger('change');
    
    function educationChange() {
        var selectedOption = $("#userDetail_EducationId").val();
        if (selectedOption === '0') {
            $("#other_education").css('display', 'block');
        }
        else {
            $("#other_education").css('display', 'none');
        }
    }

    $(".resstatus").click(function () {
        resdencyChange();
    });

    $("#userDetail_ResidencyStatus").change(function () {
        resdencyChange();
    }).trigger('change');   

    function resdencyChange() {
        var selectedOption = $("#userDetail_ResidencyStatus").val();
        if (selectedOption === 'other') {
            $("#other_residencyStatus").css('display', 'block');
        }
        else {
            $("#other_residencyStatus").css('display', 'none');
        }
    }

    $(".rel").click(function () {
        religionChange();
    });

    $("#userDetail_Religion").change(function () {
        religionChange();
    }).trigger('change');

    function religionChange() {
        var selectedOption = $("#userDetail_Religion").val();
        // console.log(selectedOption);
        if (selectedOption == '0') {
            $("#other_religion").css('display', 'block');
        }
        else {
            $("#other_religion").css('display', 'none');
        }
    }

    $(".ern").click(function () {
        currencyChange();
    });

    $("#userDetail_Currency").change(function () {
        currencyChange();
    }).trigger('change');

    function currencyChange() {
        var selectedOption = $("#userDetail_Currency").val();
        console.log(selectedOption);
        if (selectedOption === "INR") {
            $(".salary-inr").css('display', 'block');
            $(".company-ternover-inr").css('display', 'block');

            enableSelectFields("salary-inr", "salaryINR");
            enableSelectFields("company-ternover-inr", "turnoverINR");
            $("#salaryINR").attr('disabled', false);
            $("#turnoverINR").attr('disabled', false);

            $(".salary-usd").css('display', 'none');
            $(".company-ternover-usd").css('display', 'none');

            disableSelectFields("salary-usd", "salaryUSD");
            disableSelectFields("company-ternover-usd", "turnoverUSD");
            $("#salaryUSD").attr('disabled', true);
            $("#turnoverUSD").attr('disabled', true);

        }
        else {
            $(".salary-inr").css('display', 'none');
            $(".company-ternover-inr").css('display', 'none');

            disableSelectFields("salary-inr", "salaryINR");
            disableSelectFields("company-ternover-inr", "turnoverINR");
            $("#salaryINR").attr('disabled', true);
            $("#turnoverINR").attr('disabled', true);

            $(".salary-usd").css('display', 'block');
            $(".company-ternover-usd").css('display', 'block');

            enableSelectFields("salary-usd", "salaryUSD");
            enableSelectFields("company-ternover-usd", "turnoverUSD");
            $("#salaryUSD").attr('disabled', false);
            $("#turnoverUSD").attr('disabled', false);
        }
    }

    $(".res").click(function () {
        residenceCountryChange();
    });

    $("#ResidenceCountry").change(function () {
        residenceCountryChange();
    }).trigger('change');

    function residenceCountryChange() {
        var selectedOption = $("#ResidenceCountry").val();
        //console.log(selectedOption);
        $("#other_residence_country").css('display', 'none');
        $("#donno").prop('checked', true);
        if (selectedOption == '99') {
            $(".prvinr").css('display', 'block');
            $(".prminr").css('display', 'block');
            $(".prvusd").css('display', 'none');
            $(".prmusd").css('display', 'none');
            $("input[name='defaultExampleRadios']").prop("checked", false);
        }
        else if (selectedOption == '223' || selectedOption == '0') {
            if (selectedOption == '0')
                $("#other_residence_country").css('display', 'block');
            $(".prvinr").css('display', 'none');
            $(".prminr").css('display', 'none');
            $(".prvusd").css('display', 'block');
            $(".prmusd").css('display', 'block');
            $("input[name='defaultExampleRadios']").prop("checked", false);
        }
        else {
            $(".prvinr").css('display', 'none');
            $(".prminr").css('display', 'none');
            $(".prvusd").css('display', 'block');
            $(".prmusd").css('display', 'block');
            $("input[name='defaultExampleRadios']").prop("checked", false);
        }
    }

    $(".grew").click(function () {
        grewCountryChange();
    });

    $("#userDetail_GrewCountryId").change(function () {
        grewCountryChange();
    }).trigger('change');

    function grewCountryChange() {
        var selectedOption = $("#userDetail_GrewCountryId").val();
        //console.log(selectedOption);
        if (selectedOption === '0') {
            $("#other_grew_country").css('display', 'block');
        }
        else {
            $("#other_grew_country").css('display', 'none');
        }
    }

    $(".emp").click(function () {
        employmentChange();
    });

    $("#userDetail_Employment").change(function () {
        employmentChange();
    }).trigger('change');

    function employmentChange() {
        var emp = $("#userDetail_Employment").val();
        $("#other_employment").css('display', 'none');
        // console.log(emp);
        $("#companyDiv").css("display", "block");
        $("#desgDiv").css("display", "block");
        $("#ernDiv").css("display", "block");
        $("#annDiv").css("display", "block");
        $("#cannDiv").css("display", "block");
        switch (emp) {
            case "salaried":
                // console.log(emp);
                $("#cannDiv").css("display", "none");

                break;
            case "a student":
                //  console.log(emp);
                $("#companyDiv").css("display", "none");
                $("#desgDiv").css("display", "none");
                $("#ernDiv").css("display", "none");
                $("#annDiv").css("display", "none");
                $("#cannDiv").css("display", "none");
                break;
            case "other":
                $("#other_employment").css('display', 'block');
                break;

        }
    }

    $(".salary-inr").click(function () {
        //console.log("salary inr change");
        salaryChange();
    });
    $(".salary-usd").click(function () {
        //console.log("salary usd change");
        salaryChange();
    });
    
    function salaryChange() {
    var currency = $("#userDetail_Currency").val();
    if (currency == "INR") {
        $("#Salary").val($("#salaryINR").val());
    }
    else {
        $("#Salary").val($("#salaryUSD").val());
    }
    }

    $(".company-ternover-inr").click(function () {
    console.log("turnover inr change");
    turnoverChange();
    });
    $(".company-ternover-usd").click(function () {
    console.log("turnover usd change");
    turnoverChange();
    });

    function turnoverChange() {
    var currency = $("#userDetail_Currency").val();
    if (currency == "INR") {
        $("#Turnover").val($("#turnoverINR").val());
    }
    else {
        $("#Turnover").val($("#turnoverUSD").val());
    }
}

$(".calyr").click(function () {
    var dtMonth = $("#CalMonth").val();
    var dtDay = $("#CalDay").val();
    if (dtMonth != "" && dtMonth != undefined && dtDay != "" && dtDay != undefined)
        checkDate();

});

$(".calmon").click(function () {
    var dtYear = $("#CalYear").val();
    var dtDay = $("#CalDay").val();
    if (dtYear != "" && dtYear != undefined && dtDay != "" && dtDay != undefined)
        checkDate();

});

$(".caldays").click(function () {
    checkDate();

});

function checkDate() {
    var dtYear = $("#CalYear").val();
    var dtMonth = $("#CalMonth").val();
    var dtDay = $("#CalDay").val();
    $("#userMaster_DOB").parent().find(".help-block.form-error").remove();
    if (validateDate(dtYear, dtMonth, dtDay)) {

        $("#userMaster_DOB").val(dtMonth + "/" + dtDay + "/" + dtYear);
        if (!validateAge()) {
            $("#userMaster_DOB").parent().append("<span class='help-block form-error'>Age must be 18 or greater</span>");
        }
    }
    else {
        $("#userMaster_DOB").parent().append("<span class='help-block form-error'>Invalid Date</span>");
    }
}

function validateDate(dtYear,dtMonth,dtDay) {

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;

}

function validateAge() {
    var dob = new Date($("#userMaster_DOB").val());

    var today = new Date();
    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

    if (age >= 18) return true;
    else return false;
}

    $(function () {

    disableSelectFields("prvinr", "prvinrid");
    disableSelectFields("prminr", "prminrid");
    disableSelectFields("prmusd", "prmusdid");
    disableSelectFields("prvusd", "prvusdid");

    $("#verify").click(function () {
        
        $.validate({
            form: '#frmApply',
            modules: 'date',
            onError: function () {
                return false;
            },
            onSuccess: function () {
                return validateSelectFields();
            }
        });
       
    });

    function validateSelectFields() {
        var verified = 1;
        var maritalStatus = $("#userDetail_MartialStatus").val();
        var religion = $("#userDetail_Religion").val();
        var education = $("#userDetail_EducationId").val();
        var employment = $("#userDetail_Employment").val();
        var residentCountry = $("#ResidenceCountry").val();
        var residencyStatus = $("#userDetail_ResidencyStatus").val();
        var yearSpent = $("#userDetail_YearsOfResidency").val();
        var grewCountry = $("#userDetail_GrewCountryId").val();
        var dob = $("#userMaster_DOB").val();
        var selectedPackage = $("input[name='defaultExampleRadios']:checked").val();
        //var packageId = $("#userMaster_PackageId").val();
        var hearAbout = $("#userDetail_HearAboutUs").val();
        var currency = $("#userDetail_Currency").val();
        var shtml = "<span class='help-block form-error'>This is a required field</span>";
        if (maritalStatus == "" || maritalStatus == undefined) {
            verified = 0;
            $("#userDetail_MartialStatus").parent().append(shtml);
        }
        if (religion == "" || religion == undefined) {
            verified = 0;
            $("#userDetail_Religion").parent().append(shtml);
        }
        if (education == "" || education == undefined) {
            verified = 0;
            $("#userDetail_EducationId").parent().append(shtml);
        }
        if (employment == "" || employment == undefined) {
            verified = 0;
            $("#userDetail_Employment").parent().append(shtml);
        }
        if (residentCountry == "" || residentCountry == undefined) {
            verified = 0;
            $("#ResidenceCountry").parent().append(shtml);
        }
        if (residencyStatus == "" || residencyStatus == undefined) {
            verified = 0;
            $("#userDetail_ResidencyStatus").parent().append(shtml);
        }
        if (yearSpent == "" || yearSpent == undefined) {
            verified = 0;
            $("#userDetail_YearsOfResidency").parent().append(shtml);
        }
        if (grewCountry == "" || grewCountry == undefined) {
            verified = 0;
            $("#userDetail_GrewCountryId").parent().append(shtml);
        }
        //if (packageId == "" || packageId == undefined) {
        //    if (selectedPackage == "privilege") {
        //        verified = 0;
        //        $(".prvinr").parent().append(shtml);
        //    }
        //    else if (selectedPackage == "premium") {
        //        verified = 0;
        //        $(".prminr").parent().append(shtml);
        //    }
        //}
        if (hearAbout == "" || hearAbout == undefined) {
            verified = 0;
            $("#userDetail_HearAboutUs").parent().append(shtml);
        }
        if (dob == "" || dob == undefined) {
            verified = 0;
            $("#userMaster_DOB").parent().append(shtml);
        }
        else {
            if (!validateAge()) {
                verified = 0;
                $("#userMaster_DOB").parent().append("<span class='help-block form-error'>Age must be 18 or greater</span>");
            }
        }
        var salary;
        var turnover;
        if (currency == "INR") {
            salary = $("#salaryINR").val();
            turnover = $("#turnoverINR").val();
        }
        else {
            salary = $("#salaryUSD").val();
            turnover = $("#turnoverUSD").val();
        }

        switch (employment.toLowerCase()) {

            case "salaried":
                if (salary == "" || salary == undefined ) {
                    verified = "0";
                    if (currency == "INR") {
                        $("#salaryINR").parent().append(shtml);
                    }
                    else {
                        $("#salaryUSD").parent().append(shtml);
                    }
                }
                break;
            case "an entrepreneur":
            case "involved in my family business":
                if (turnover == "" || turnover == undefined) {
                    verified = "0";
                    if (currency == "INR") {
                        $("#turnoverINR").parent().append(shtml);
                    }
                    else {
                        $("#turnoverINR").parent().append(shtml);
                    }
                }
                break;
        }

        if (verified == 0) return false;
        else return true;
    }

    $("input[name='pkgExampleRadios']").change(function (e) {
            var selectedCountry = $("#ResidenceCountry").val();
        var selectedPackage = $("input[name='pkgExampleRadios']:checked").val();
    //console.log(selectedCountry);
    // console.log(selectedPackage);
        disableSelectFields("prvinr", "prvinrid");
        disableSelectFields("prvusd", "prvusdid");
        disableSelectFields("prminr", "prminrid");
        disableSelectFields("prmusd", "prmusdid");
        

        if (selectedCountry == '0' || selectedCountry == '223') {
            if (selectedPackage == 'privilege') {
                enableSelectFields("prvusd", "prvusdid");
            }
            else if (selectedPackage == 'premium') {
                enableSelectFields("prmusd", "prmusdid");
            }
        } else {
            if (selectedPackage == 'privilege') {
                enableSelectFields("prvinr", "prvinrid");
            }
            else if (selectedPackage == 'premium') {
                enableSelectFields("prminr", "prminrid");
            }
        }
    });

        $("#phone").focusout(function () {
            var input = document.querySelector('#phone');
            var iti = window.intlTelInputGlobals.getInstance(input);
            $("#PhoneNo").val(iti.getNumber());
        });

        $("#datepicker").focusin(function () {
            $(this).data("kendoDatePicker").open();
        });
    });

    function disableSelectFields(arg1, arg2) {
    $("." + arg1).removeClass("custom-select");
    $("#" + arg2).prop("disabled", true);
    $("." + arg1).children().hide();
    $("#" + arg2).show();
    }

    //arg1: class name, arg2: id of selector
    function enableSelectFields(arg1, arg2) {
    $("." + arg1).addClass("custom-select");
    $("#" + arg2).prop("disabled", false);
    $("." + arg1).children().css('display', '');
    }

    function customizeSelectFields() {
    /*************************/
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 0; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);

}   
