<style>
    .main-footer {
        padding: 0;
        box-shadow: 0px 0px 4px #414042;
    }
    .footer-container {
        width: 90%;
        margin: 0 auto;
        padding: 15px 0;
    }
    .copyright {
        text-align: center;
    }
    .main-footer ul {
        list-style-type: none;
        padding: 0px;
    }
    .main-footer ul li:first-child, .div-label {
        color: #6dbab0;
        font-size: 25px;
        font-weight: bold;
    }
    footer a {
        display: inline-block;
        color: #414042;
    }
    footer a:hover {
        color: #6dbab0;
        font-weight: 700;
        text-decoration: none;
    }
    .copyright p {
        margin: 0;
        padding: 0;
        font-size: 14px;
    }
    .social a {
        margin: 0 20px;
        color: #6dbab0;
        font-size: 35px;
    }
    .social a:hover {
        color: #000000;
    }
</style>

<footer class="main-footer">
    <div class="footer-container">
        <div class="row">
            <?php /*
            <div class="col-md-12 text-center">
                <a class="footer-logo" href="<?=base_url()?>">
                    <?php
                        $footer_logo_info = $this->db->get_where('frontend_settings', array('type' => 'footer_logo'))->row()->value;
                        $footer_logo = json_decode($footer_logo_info, true);
                        if (file_exists('uploads/footer_logo/'.$footer_logo[0]['image'])) {
                    ?>
                        <img src="<?=base_url()?>uploads/footer_logo/<?=$footer_logo[0]['image']?>" class="img-fluid" alt="">
                    <?php
                        }
                        else {
                        ?>
                            <img src="<?=base_url()?>uploads/footer_logo/default_image.png" class="img-fluid" alt="">
                        <?php
                        }
                    ?>
                  </a>
            </div>
            */ ?>
            <?php
            $this->db->select('*');
            $this->db->from('plan');
            $query = $this->db->get();
            $plan = $query->result_array();
            ?>
              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <li>Packages</li>
                      <?php
                      foreach ($plan as $pl) 
                      {
                      ?>
                      <li><a href="<?=base_url('membership')?>"><?php echo $pl['name']; ?></a></li>
                      <?php
                      }
                      ?>
                      <!-- <li><a href="<?=base_url('membership')?>">The Courtship Package</a></li>
                      <li><a href="<?=base_url('membership')?>">The Soul Mate Package</a></li> -->
                </ul>
              </div>
              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <li>Know More</li>
                      <li><a href="<?=base_url('bespoke-services')?>">Our Services</a></li>
                      <li><a href="<?=base_url('our-promise')?>">Our Promise</a></li>
                      <li><a href="<?=base_url('contact-us')?>">Contact Us</a></li>
                  </ul>
              </div>
              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <li>Legal</li>
                      <li><a href="<?=base_url('privacy-policy')?>">Privacy Policy</a></li>
                      <li><a href="<?=base_url('terms-conditions')?>">Terms And Conditions</a></li>
                      
                  </ul>
              </div>
              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <!-- <li>Get in touch</li>
                      <li><a href="#">Contact support</a></li> -->
                      <li class="div-label">Follow us on social</li>
                      <li class="social">
                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" title="Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="copyright">
              <p>All Rights Reserved © <?=date('Y')?>. <a href="<?=base_url()?>" class="" target="_blank">Mujse Shaadi Karoge</a>. Designed & Developed by <a href="https://www.anantsoftcomputing.com/" class="" target="_blank">Anant Soft Computing</a></p>
          </div>
    </div>
</footer>

                    </div>

                </div>

            </div>

            <!-- END: st-pusher -->

        </div>

        <!-- END: st-pusher -->

    </div>

    <!-- END: st-container -->

</div>
<script>
    $(document).ready(function(){
        if (isloggedin != "") {
            var noti_count = "<?php if (!empty($noti_counter)){echo $noti_counter;}?>";
            if (noti_count > 0) {
                $('.noti_counter').show();
                $('.noti_counter').html(noti_count);
            }
            var msg_count = "<?php if (!empty($msg_counter)){echo $msg_counter;}?>";
            if (msg_count > 0) {
                $('.msg_counter').show();
                $('.msg_counter').html(msg_count);
            }
        }

        $(".noti_click").click(function(){
            var member_id = "<?=$this->session->userdata('member_id')?>";
            if (member_id != "") {
                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>home/refresh_notification/"+member_id,
                    cache: false,
                    success: function(response) {
                        $('.noti_counter').hide();
                        // $('#test').html(response);
                    }
                });
            }
        });
    });
</script>
<!-- END: body-wrap --> 