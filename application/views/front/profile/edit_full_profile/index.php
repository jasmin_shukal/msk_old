
<script src="<?=base_url()?>template/front/js/jquery.inputmask.bundle.min.js"></script>

<?php include_once APPPATH.'views/front/profile/profile_nav.php';?>

<img style="position: absolute;" src="<?=base_url('new_template/images/katputli-both.png')?>" width="250" height="250">

<section class="slice ">

    <div class="profile">

        <div class="container">
            <?php foreach ($get_member as $member){ ?>

                <div class="row cols-md-space cols-sm-space cols-xs-space">

                    <div class="col-lg-4">

                        <?php include_once APPPATH.'views/front/profile/left_panel.php';?>

                    </div>

                    <div class="col-lg-8">

                        <form id="edit_profile_form" class="form-default" role="form" action="<?=base_url()?>home/profile/update_all" method="POST">

                            <div class="widget">

                                <div class="card z-depth-2-top" id="profile_load">

                                    <?php if (!empty(validation_errors())): ?>

                                        <div class="widget" id="profile_error">

                                            <div style="border-bottom: 1px solid #e6e6e6;">

                                                <div class="card-title" style="padding: 0.5rem 1.5rem; color: #fcfcfc; background-color: #de1b1b; border-top-right-radius:0.25rem; border-top-left-radius:0.25rem;">You <b>Must Provide</b> the Information(s) bellow</div>

                                                <div class="card-body" style="padding: 0.5rem 1.5rem; background: rgba(222, 27, 27, 0.10);">

                                                    <style>

                                                        #profile_error p {

                                                            color: #DE1B1B !important; margin: 0px !important; font-size: 12px !important;

                                                        }

                                                    </style>

                                                    <?= validation_errors();?>

                                                </div>

                                            </div>

                                        </div>

                                    <?php

                                        endif;

                                    ?>

                                    <div class="card-title">

                                        <h3 class="heading heading-6 strong-500 pull-left">

                                            <b><?php echo translate('edit_profile_informations')?></b>

                                        </h3>

                                        <div class="pull-right">

                                            <button type="submit" class="btn btn-base-1 btn-rounded btn-sm btn-shadow"><i class="ion-checkmark"></i> <?php echo translate('update')?></button>

                                            <a href="<?=base_url()?>home/profile" class="btn btn-danger btn-sm btn-shadow"><i class="ion-close"></i> <?php echo translate('cancel')?></a>

                                        </div>

                                    </div>

                                    <div class="card-body" style="padding: 1.5rem 0.5rem;">

                                        <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">

                                            <div id="edit_introduction">

                                                <div class="card-inner-title-wrapper  pt-0">

                                                    <h3 class="card-inner-title pull-left">

                                                    <?php echo translate('introduction')?></h3>

                                                </div>

                                                <div class='clearfix'>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="form-group has-feedback">

                                                            <textarea name="introduction" class="form-control no-resize" rows="6"><?php if(!empty($form_contents)){echo $form_contents['introduction'];} else{echo $member['introduction'];}?></textarea>

                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                                            <div class="help-block with-errors">

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                        <!-- foreach Start -->
                                        <?php
                                            $this->db->select('*');
                                            $this->db->from('profile_main_category');
                                            $this->db->where('value','Yes');
                                            $this->db->order_by('pm_cat_id','ASC');
                                            $profile_section = $this->db->get()->result_array();
                                            foreach ($profile_section as $prsect) {
                                        ?>
                                        <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">
                                            <div id="edit_<?=$prsect['pm_cat_slug']?>">
                                                <div class="card-inner-title-wrapper  pt-0">
                                                    <h3 class="card-inner-title pull-left"><?=$prsect['pm_cat_name']?></h3>
                                                </div>
                                                <div class='clearfix'></div>
                                            </div>
                                            <div class="row">
                                                <?php
                                                    $obj = json_decode($member[$prsect['pm_cat_slug']]);
                                                    $arr = (array)$obj;
                                                    
                                                    $this->db->select('*');
                                                    $this->db->from('profile_field');
                                                    $this->db->where('profile_section_id',$prsect['pm_cat_id']);
                                                    $this->db->where('profile_field_value','Yes');
                                                    $this->db->order_by('profile_field_id','ASC');
                                                    $field_data = $this->db->get()->result_array();
                                                    foreach ($field_data as $prfld) {
                                                ?>
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <label for="first_name" class="text-uppercase c-gray-light"><?=$prfld['profile_field_name']?></label>
                                                        <?php
                                                            if ($prfld['profile_field_type']=='text') {
                                                        ?>
                                                        <input type="text" class="form-control no-resize" name="<?=strtolower(str_replace(" ", "_", $prfld['profile_field_name']))?>" value="<?php if($arr[$prfld['profile_field_name']]!='') { echo $arr[$prfld['profile_field_name']]; } ?>">
                                                        <?php
                                                            }
                                                            if($prfld['profile_field_type']=='drop')
                                                            {
                                                        ?>
                                                        <select class="form-control form-control-sm selectpicker present_state_f_edit" name="<?=strtolower(str_replace(" ", "_", $prfld['profile_field_name']))?>">
                                                            <option value="">Choose <?=ucwords(strtolower($prfld['profile_field_name']))?></option>
                                                        </select>
                                                        <?php
                                                            }
                                                            if($prfld['profile_field_type']=='date')
                                                            {
                                                        ?>
                                                        <input type="date" class="form-control no-resize" name="<?=strtolower(str_replace(" ", "_", $prfld['profile_field_name']))?>" value="<?php if($arr[$prfld['profile_field_name']]!='') { echo $arr[$prfld['profile_field_name']]; } ?>">
                                                        <?php
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        ?>
                                        <!-- foreach End -->



                                        <div class="col-8 ml-auto mr-auto">

                                            <div class="row text-center">

                                                <div class="col-6">

                                                    <a href="<?=base_url()?>home/profile" class="btn btn-danger btn-sm z-depth-2-bottom" style="width: 100%"><?php echo translate('cancel')?></a>

                                                </div>

                                                <div class="col-6">

                                                    <button type="submit" class="btn btn-base-1 btn-rounded btn-sm z-depth-2-bottom btn-icon-only" style="width: 100%"><?php echo translate('update')?></button>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            <?php } ?>

        </div>

    </div>

</section>

<script>

    $(document).ready(function(){

        $(".height_mask").inputmask({

            mask: "9.99",

            greedy: false,

            definitions: {

                '*': {

                    validator: "[0-9]"

                }

            }

        });

    });

</script>

<script>

    $(".present_country_f_edit").change(function(){

        var country_id = $(".present_country_f_edit").val();

        if (country_id == "") {

            $(".present_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");

            $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".present_state_f_edit").html(data);

                    $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });

    $(".present_state_f_edit").change(function(){

        var state_id = $(".present_state_f_edit").val();

        if (state_id == "") {

            $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id/city/state_id/"+state_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".present_city_f_edit").html(data);

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });



    $(".permanent_country_f_edit").change(function(){

        var country_id = $(".permanent_country_f_edit").val();

        if (country_id == "") {

            $(".permanent_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");

            $(".permanent_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".permanent_state_f_edit").html(data);

                    $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });

    $(".permanent_state_f_edit").change(function(){

        var state_id = $(".permanent_state_f_edit").val();

        if (state_id == "") {

            $(".permanent_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id/city/state_id/"+state_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".permanent_city_f_edit").html(data);

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });

    $(".prefered_country_f_edit").change(function(){

        var country_id = $(".prefered_country_f_edit").val();

        if (country_id == "") {

            $(".prefered_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".prefered_state_f_edit").html(data);

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });





    $(".present_religion_f_edit").change(function(){

        var religion_id = $(".present_religion_f_edit").val();

        if (religion_id == "") {

            $(".present_caste_f_edit").html("<option value=''><?php echo translate('choose_a_religion_first')?></option>");

            $(".present_sub_caste_f_edit").html("<option value=''><?php echo translate('choose_a_caste_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id_caste/caste/religion_id/"+religion_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".present_caste_f_edit").html(data);

                    $(".present_sub_caste_f_edit").html("<option value=''><?php echo translate('choose_a_caste_first')?></option>");

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });

    $(".present_caste_f_edit").change(function(){

        var caste_id = $(".present_caste_f_edit").val();

        if (caste_id == "") {

            $(".present_sub_caste_f_edit").html("<option value=''><?php echo translate('choose_a_caste_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id_caste/sub_caste/caste_id/"+caste_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".present_sub_caste_f_edit").html(data);

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });



    $(".prefered_religion_edit").change(function(){

        var religion_id = $(".prefered_religion_edit").val();

        if (religion_id == "") {

            $(".prefered_caste_edit").html("<option value=''><?php echo translate('choose_a_religion_first')?></option>");

            $(".prefered_sub_caste_edit").html("<option value=''><?php echo translate('choose_a_caste_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id_caste/caste/religion_id/"+religion_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".prefered_caste_edit").html(data);

                    $(".prefered_sub_caste_edit").html("<option value=''><?php echo translate('choose_a_caste_first')?></option>");

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });

    $(".prefered_caste_edit").change(function(){

        var caste_id = $(".prefered_caste_edit").val();

        if (caste_id == "") {

            $(".prefered_sub_caste_edit").html("<option value=''><?php echo translate('choose_a_caste_first')?></option>");

        } else {

            $.ajax({

                url: "<?=base_url()?>home/get_dropdown_by_id_caste/sub_caste/caste_id/"+caste_id, // form action url

                type: 'POST', // form submit method get/post

                dataType: 'html', // request type html/json/xml

                cache       : false,

                contentType : false,

                processData : false,

                success: function(data) {

                    $(".prefered_sub_caste_edit").html(data);

                },

                error: function(e) {

                    console.log(e)

                }

            });

        }

    });

</script>

