<!-- <!DOCTYPE html> -->
 <!-- <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="Mujse Shaadi Karoge ,Matrimonial">
        <meta name="author" content="Divyang Patel">
        <meta name="revisit-after" content="2 day(s)">
        <script src="<?=base_url('template/front/vendor/pace/js/pace.min.js') ?>"></script>
        <link rel="stylesheet" href="<?=base_url('template/front/vendor/pace/css/pace-minimal.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/vendor/bootstrap/css/bootstrap.min.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/vendor/swiper/css/swiper.min.css') ?>">
        <link rel="stylesheet" href="<?=base_url('template/front/vendor/hamburgers/hamburgers.min.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/vendor/animate/animate.min.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/vendor/lightgallery/css/lightgallery.min.css') ?>">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/font-awesome/css/font-awesome.min.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/ionicons/css/ionicons.min.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/line-icons/line-icons.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/line-icons-pro/line-icons-pro.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/linea/arrows/linea-icons.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/linea/basic/linea-icons.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/linea/ecommerce/linea-icons.css') ?>" type="text/css">
        <link rel="stylesheet" href="<?=base_url('template/front/fonts/linea/software/linea-icons.css') ?>" type="text/css">
        <link id="stylesheet" type="text/css" href="<?=base_url('template/front/css/global-style-purple.css') ?>" rel="stylesheet" media="screen">
        <link type="text/css" href="<?=base_url('template/front/css/custom-style.css') ?>" rel="stylesheet">
        <style>
        #logo{ 
            position:fixed; 
            top:0; 
            left:0; 
        }
       </style>
    </head>
    <body>
        <div class="body-wrap">
            <div id="st-container" class="st-container">
                <div class="st-pusher">
                    <div class="st-content">
                        <div class="st-content-inner">
                            <div class="sticky-content">
                                <section class="slice">
                                   <img style="position: absolute;" src="<?=base_url('new_template/images/katputli-both.png')?>" width="120" height="120">
                                    <div class="profile">
                                        <div class="container">
                                            <div class="row cols-md-space cols-sm-space cols-xs-space">
                                                <div class="col-lg-4">
                                                    <div class="sidebar sidebar-inverse sidebar--style-1 bg-base-1 z-depth-2-top">
                                                        <div class="sidebar-object mb-0">
                                                            <div class="profile-picture profile-picture--style-2">
                                                                <div style="border: 10px solid rgba(255, 255, 255, 0.1);width: 200px;border-radius: 50%;margin-top: 30px;" class="mx-auto">
                                                                    <div class="profile_img" id="show_img" style="background-image: url(<?=base_url('uploads/profile_image/'.$arr['profile_image'])?>)"></div>
                                                                </div>
                                                            </div>
                                                            <div class="profile-details">
                                                                <h2 class="heading heading-3 strong-500 profile-name"><?=$get_member['first_name']." ".$get_member['last_name'] ?></h2>
                                                                <h3 class="heading heading-6 strong-400 profile-occupation mt-3"></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="widget">
                                                        <div class="card z-depth-2-top" id="profile_load">
                                                            <div class="card-title">
                                                                <h3 class="heading heading-6 strong-500 pull-left"><b>Profile Information</b></h3>
                                                                 <div class="pull-right">
                                                                    <img src="<?=base_url('new_template/images/logo.png') ?>"width="100" height="70">
                                                                </div> 
                                                            </div>
                                                            <div class="card-body pt-2" style="padding: 1rem 0.5rem;">
                                                                <!-- Contact information -->
                                                               <!--  <div id="section_introduction">
                                                                    <div class="mb-2 pl-3">
                                                                        <b>Member ID - </b><b class="c-base-1"><?=$get_member['member_profile_id'] ?></b>
                                                                    </div>
                                                                    <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">
                                                                        <div id="info_introduction">
                                                                            <div class="card-inner-title-wrapper pt-0">
                                                                                <h3 class="card-inner-title pull-left">
                                                                                    Introduction
                                                                                </h3>
                                                                            </div>
                                                                            <div class="table-full-width">
                                                                                <div class="table-full-width">
                                                                                    <table class="table table-profile table-responsive table-slick">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="">
                                                                                                    <?=$get_member['introduction'] ?>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                    $this->db->select('*');
                                                                    $this->db->from('profile_main_category');
                                                                    $this->db->where('value','Yes');
                                                                    $this->db->order_by('pm_cat_id','ASC');
                                                                    $profile_section = $this->db->get()->result_array();
                                                                    foreach ($profile_section as $prsect) {
                                                                ?>
                                                                <div id="section_<?=$prsect['pm_cat_slug']?>">
                                                                    <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">
                                                                        <div id="info_<?=$prsect['pm_cat_slug']?>">
                                                                            <div class="card-inner-title-wrapper pt-0">
                                                                                <h3 class="card-inner-title pull-left"><?=$prsect['pm_cat_name']?></h3>
                                                                            </div>
                                                                            <div class="table-full-width">
                                                                                <div class="table-full-width">
                                                                                    <table class="table table-profile table-responsive table-striped table-bordered table-slick">
                                                                                        <tbody>
                                                                                            <?php
                                                                                                $obj = json_decode($get_member[$prsect['pm_cat_slug']]);
                                                                                                $arr = (array)$obj;
                                                                                                
                                                                                                $this->db->select('*');
                                                                                                $this->db->from('profile_field');
                                                                                                $this->db->where('profile_section_id',$prsect['pm_cat_id']);
                                                                                                $this->db->where('profile_field_value','Yes');
                                                                                                $this->db->order_by('profile_field_id','ASC');
                                                                                                $field_data = $this->db->get()->result_array();
                                                                                                $i=0;
                                                                                                foreach ($field_data as $prfld) {
                                                                                                    $i++;
                                                                                                    if($i==1){ echo "<tr>"; }
                                                                                            ?>
                                                                                                <td class="td-label">
                                                                                                    <span><?=$prfld['profile_field_name']?></span>
                                                                                                </td>
                                                                                                <td><?php if($arr[$prfld['profile_field_name']]!='') { echo $arr[$prfld['profile_field_name']]; } ?></td>
                                                                                            <?php if($i==2){ echo "</tr>"; $i=0; } ?>
                                                                                            <?php } ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?=base_url('template/front/vendor/popper/popper.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?=base_url('template/front/js/vendor/jquery.easing.js') ?>"></script>
        <script src="<?=base_url('template/front/js/ie10-viewport-bug-workaround.js') ?>"></script>
        <script src="<?=base_url('template/front/js/slidebar/slidebar.js') ?>"></script>
        <script src="<?=base_url('template/front/js/classie.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/bootstrap-dropdown-hover/js/bootstrap-dropdown-hover.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/bootstrap-notify/bootstrap-growl.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/scrollpos-styler/scrollpos-styler.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/flatpickr/flatpickr.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/easy-pie-chart/jquery.easypiechart.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/footer-reveal/footer-reveal.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/sticky-kit/sticky-kit.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/swiper/js/swiper.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/paraxify/paraxify.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/viewport-checker/viewportchecker.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/milestone-counter/jquery.countTo.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/countdown/js/jquery.countdown.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/typed/typed.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/instafeed/instafeed.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/gradientify/jquery.gradientify.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/nouislider/js/nouislider.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/isotope/isotope.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/imagesloaded/imagesloaded.pkgd.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/lightgallery/js/lightgallery.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/lightgallery/js/lg-thumbnail.min.js') ?>"></script>
        <script src="<?=base_url('template/front/vendor/lightgallery/js/lg-video.js') ?>"></script>
        <script src="<?=base_url('template/front/js/wpx.app.js') ?>"></script>
    </body> --> 
<!-- </html> -->  