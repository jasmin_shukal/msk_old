<?php
    $this->db->select('*');
    $this->db->from('member');
    $this->db->where('member_id',$member_id);
    $get_member = $this->db->get()->row_array();
    $profile = json_decode($get_member['profile_image']);
    $arr = (array)$profile[0];
?>
<!DOCTYPE html>
<html>
<head>
    <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        td, th {
          border: 1px solid #dddddd;
          
          
        }

        tr:nth-child(even) {
          background-color: #dddddd;
        }    
    </style>
</head>
<body>
    <table align="center" class="col-md-12">
        <caption><strong>PROFILE INFORMATION</strong></caption>
      <tr>
        <th>Profile</th>
        <th>Introduction</th>
      </tr>
      <tr>
        <td style="width: 50%; text-align: center;"><img src="<?php echo base_url() ?>uploads/profile_image/<?php echo $arr['profile_image'];?>" height="100px" width="100px" style="border-radius: 50%"></td>
        <td style="width: 50%;"><strong><?=$get_member['first_name']." ".$get_member['last_name'] ?></strong><br><?=$get_member['introduction'] ?></td>
      </tr>
    </table>
    <?php
        $this->db->select('*');
        $this->db->from('profile_main_category');
        $this->db->where('value','Yes');
        $this->db->order_by('pm_cat_id','ASC');
        $profile_section = $this->db->get()->result_array();
        foreach ($profile_section as $prsect) {
        ?>
<table style="margin-top: 15px" align="center">
    <tr>
    <th colspan="4"><?=$prsect['pm_cat_name']?></th>
    </tr>
    <?php
        $object = json_decode($get_member[$prsect['pm_cat_slug']]);
        $array = (array)$object;
                                                                                                    
        $this->db->select('*');
        $this->db->from('profile_field');
        $this->db->where('profile_section_id',$prsect['pm_cat_id']);
        $this->db->where('profile_field_value','Yes');
        $this->db->order_by('profile_field_id','ASC');
        $field_data = $this->db->get()->result_array();
        $i=0;
        foreach ($field_data as $prfld) {
            $i++;
            if($i==1){ echo "<tr>"; }
    ?>
        <td style="width: 25%"><?=$prfld['profile_field_name']?></td>
        <td style="width: 25%"><?php if($array[$prfld['profile_field_name']]!='') { echo $array[$prfld['profile_field_name']]; } ?></td>
    <?php if($i==2){ echo "</tr>"; $i=0; } ?>
    <?php } ?>
</table>
<?php } ?>