<div class="card-title">

    <h3 class="heading heading-6 strong-500">

    <?php $cm = new Crud_model(); ?>    

    <b><?php echo translate('Matching Profiles')?></b></h3>

</div>
<div class="card-body">

    <section class="sct-color-1 pricing-plans pricing-plans--style-1">

        <div class="container">

            <span class="clearfix"></span>

            <div class="row">

                <div class="col-md-12">

                    <div class="block">

                        <?php

                           /* $package_info = $this->db->get_where('member', array('member_id' => $this->session->userdata('member_id')))->row()->package_info;

                            $package_info = json_decode($package_info, true);*/

                        ?>
                        

                        <h2 class="plan-title text-uppercase strong-600"><?php echo translate('Matching')?></h2>

                        <div class="text-center">

                            <div class="px-2 py-2 text-left">

                                <table class="table table-condensed table-bordered">

                                    <tbody>

                                        <tr>

                                            <th><?php echo translate('Profile id')?></th>

                                            <th><?php echo translate('Name')?></th>

                                            <th><?php echo translate('Profile Download')?></th>

                                        </tr>
                                            <?php
                                                $this->db->select('*');
                                                $this->db->from('matching');
                                                $this->db->where('main_id',$this->session->userdata('member_id'));
                                                $profile_section = $this->db->get()->result_array();
                                                foreach ($profile_section as $prsect) {
                                            ?>
                                       <tr>
                                           <td><?php echo $prsect['profile_id']; ?></td>
                                           <td><?php echo ucwords($cm->get_member_param_by_id($prsect['profile_id'],'first_name')." ".$cm->get_member_param_by_id($prsect['profile_id'],'last_name')); ?></td>
                                           <td><a target="_blank" href="<?=base_url()?>home/show_pdf/<?php echo $prsect['profile_id']; ?>" class="btn btn-danger">Download</a></td>
                                       </tr>
                                       <?php
                                        }
                                       ?>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

                

            </div>

        </div>

    </section>

</div>