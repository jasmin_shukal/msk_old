<p class="text-main text-semibold"><?php echo translate('faqs')?></p>

<form class="form-horizontal" id="faqs_form" method="POST" action="<?=base_url()?>admin/save_frontend_settings/faqs_edit">

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:1')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q1" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:1')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans1" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:2')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q2" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:2')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans2" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:3')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q3" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:3')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans3" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:4')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q4" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:4')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans4" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:5')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q5" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:5')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans5" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:6')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q5" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:6')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans6" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:7')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q7" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:7')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans7" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:8')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q8" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:8')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans8" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:9')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q9" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:9')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans9" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:10')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q10" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:10')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans10" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:11')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q11" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:11')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans11" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:12')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q12" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:12')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans12" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:13')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q13" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:13')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans13" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:14')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q14" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:14')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans14" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:15')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q15" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:15')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans15" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:16')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q16" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:16')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans16" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:17')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q17" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:17')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans17" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:18')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q18" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:18')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans18" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:19')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q19" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:19')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans19" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:20')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q20" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:20')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans20" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:21')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q21" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:21')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans21" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:22')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q22" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:22')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans22" class="form-control"  id="faqs_form" value="">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Q:23')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="q23" class="form-control"  id="faqs_form" value="">

        </div>

	</div>


	<div class="form-group">

		<label class="col-sm-2 control-label" for="faqs_form"><b><?php echo translate('Ans:23')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="ans23" class="form-control"  id="faqs_form" value="">

        </div>

	</div>





    <div class="form-group">

		<div class="col-sm-offset-2 col-sm-9">

        	<button type="submit" class="btn btn-primary btn-sm btn-labeled fa fa-save"><?php echo translate('edit')?></button>

		</div>

	</div>



		
	

</form>
