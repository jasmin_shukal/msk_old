<p class="text-main text-semibold"><?php echo translate('bespoke_services')?></p>

<form class="form-horizontal" id="add_services_form" method="POST" action="<?=base_url()?>admin/save_frontend_settings/update_services">

	<div class="form-group">

		<label class="col-sm-2 control-label" for="add_services"><b><?php echo translate('services main description')?></b></label>

        <div class="col-sm-9">

        	<textarea class="form-control" id="add_services" name="main_desc" rows="5"><?=$bespoke_services['main_desc']?></textarea>

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="add_services"><b><?php echo translate('Tab 1 Heading')?></b></label>

        <div class="col-sm-9">

        	<input type="hidden" class="form-control" name="id" value="<?=$bespoke_services['id']?>">

        	<input type="text" name="name1" class="form-control"  id="add_services" value="<?=$bespoke_services['tab_f_name']?>">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="add_services"><b><?php echo translate('Astrology Description')?></b></label>

        <div class="col-sm-9">

        	<textarea class="form-control" id="add_services" name="astro_desc" rows="5"><?=$bespoke_services['astro_desc']?></textarea>

        </div>

	</div>
	<div class="form-group">

		<label class="col-sm-2 control-label" for="add_services"><b><?php echo translate('Tab 2 Heading')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="name2" class="form-control"  id="add_services" value="<?=$bespoke_services['tab_s_name']?>">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="add_services"><b><?php echo translate('Grooming Description')?></b></label>

        <div class="col-sm-9">

        	<textarea class="form-control" id="add_services" name="grooming_desc" rows="5"><?=$bespoke_services['grooming_desc']?></textarea>

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="add_services"><b><?php echo translate('Tab 3 Heading')?></b></label>

        <div class="col-sm-9">

        	<input type="text" name="name3" class="form-control"  id="add_services" value="<?=$bespoke_services['tab_t_name']?>">

        </div>

	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label" for="add_services"><b><?php echo translate('Events Description')?></b></label>

        <div class="col-sm-9">

        	<textarea class="form-control" id="add_services" name="events_desc" rows="5"><?=$bespoke_services['events_desc']?></textarea>

        </div>

	</div>

	<div class="form-group">

		<div class="col-sm-offset-2 col-sm-9">

        	<button type="submit" class="btn btn-primary btn-sm btn-labeled fa fa-save"><?php echo translate('edit')?></button>

		</div>

	</div>

		
	

</form>
