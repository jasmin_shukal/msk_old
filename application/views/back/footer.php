<!-- FOOTER -->

<!--===================================================-->

<footer id="footer">

	<p class="text-center">

		&#0169; <?=date("Y")?> <a href="<?=base_url()?>" target="_blank"><?=$this->system_name?></a> | Designed & Developed by <a href="https://www.anantsoftcomputing.com/" target="_blank">Anant Soft Computing</a>

	</p>

</footer>

<!--===================================================-->

<!-- END FOOTER -->

