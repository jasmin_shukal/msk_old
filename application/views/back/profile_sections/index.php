<!--CONTENT CONTAINER-->

<!--===================================================-->

<div id="content-container">

	<div id="page-head">

		<!--Page Title-->

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<div id="page-title">

			<h1 class="page-header text-overflow"><?= translate('profile_sections')?></h1>



		</div>

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<!--End page title-->

		<!--Breadcrumb-->

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<ol class="breadcrumb">

			<li><a href="#"><?= translate('home')?></a></li>

			<li><a href="#"><?= translate('configuration')?></a></li>

			<li><a href="#"><?= translate('profile_sections')?></a></li>

		</ol>

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<!--End breadcrumb-->

	</div>

	<!--Page content-->

	<!--===================================================-->

	<div id="page-content">

		<div class="panel">

			<?php if (!empty($success_alert)): ?>

				<div class="alert alert-success" id="success_alert" style="display: block">

	                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>

	                <?=$success_alert?>

	            </div>

			<?php endif ?>

			<?php if (!empty($danger_alert)): ?>

				<div class="alert alert-danger" id="danger_alert" style="display: block">

	                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>

	                <?=$danger_alert?>

	            </div>

			<?php endif ?>

			<div class="panel-heading">

				<h3 class="panel-title"><?= translate('profile_sections_configuration')?></h3>

			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<form class="form-horizontal" id="profile_sections_settings_form" method="POST" action="<?=base_url()?>admin/update_profile_sections_settings">
			    			<?php
							    foreach ($get_profile_section as $val)
							    {
			            	?>
			            	
				            	
			    			<div class="form-group">

			    				<div class="col-sm-2 text-right">

									<div class="checkbox">

						                <input id="<?=$val['pm_cat_slug']?>_status" name="<?=$val['pm_cat_slug']?>_status" class="magic-checkbox" type="checkbox" <?php if($val['value'] == "yes"){ ?>checked<?php } ?>>

						                <label for="<?=$val['pm_cat_slug']?>_status"></label>

		                            </div>

								</div>

								<label class="col-sm-5 control-label text-left" for="<?=$val['pm_cat_slug']?>_status"><b><?=$val['pm_cat_name']?></b> 
		                           
		                       </label>
		                         
		                         <div class="col-sm-1">
								   <i onclick="show_attribute_form('<?=$val['pm_cat_slug']?>')" class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</div>
								


							</div>
								
							<?php
				                }
		             		?>
		             		<div class="form-group">
								<div class="col-sm-8 text-center">
						        	<button type="submit" class="btn btn-primary btn-sm btn-labeled fa fa-save">Save Profile Sections</button>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-8">
						<?php
						    foreach ($get_profile_section as $val)
						    {
						    	$this->db->select('*');
						    	$this->db->from('profile_field');
						    	$this->db->where('profile_section_id',$val['pm_cat_id']);
						    	$this->db->order_by('profile_field_id','ASC');
						    	$field_data = $this->db->get()->result_array();
		            	?>
						<div style="display: none;" class="attribute_form panel" id="<?=$val['pm_cat_slug']?>_form">
							<h3 class="text-center">Edit Fields of <?=$val['pm_cat_name']?></h3>
							<form class="form-horizontal" id="<?=$val['pm_cat_slug']?>_form" method="POST" action="<?=base_url()?>admin/update_profile_fields/<?=$val['pm_cat_slug']?>">
								<div class="row" style="margin-bottom: 10px;">
									<div class="col-md-4 text-center">
						        		<button class="btn btn-success" type="button" onclick="field_changes('<?=$val['pm_cat_slug']?>','add')"><i class="fa fa-plus-circle"></i> Add Field</button>
									</div>
									<div class="col-md-6 text-center">
										<button class="btn btn-danger" type="button" onclick="field_changes('<?=$val['pm_cat_slug']?>','remove')"><i class="fa fa-minus-circle"></i> Remove Field</button>
									</div>
						        </div>
						        <?php
						        	if(count($field_data)>0)
						        	{
						        ?>
						        <div class="form-group" id="<?=$val['pm_cat_slug']?>_field_data">
									<input type="hidden" name="<?=$val['pm_cat_slug']?>_no_of_fields" id="<?=$val['pm_cat_slug']?>_no_of_fields" value="<?=count($field_data)?>">
									<?php
										for ($i=1; $i <= count($field_data); $i++) {
											$j=$i-1;
											$chk='';
											if($field_data[$j]['profile_field_value'] == "Yes"){ $chk = "checked"; }
									?>
									<div class="row" style="margin-bottom: 10px;" id="<?=$val['pm_cat_slug']?>_row<?=$i?>">
										<div class="col-sm-1 text-right">
											<div class="checkbox">
								                <input id="<?=$val['pm_cat_slug']?>_add_field<?=$i?>_val" name="<?=$val['pm_cat_slug']?>_add_field<?=$i?>_val" class="magic-checkbox" type="checkbox" <?=$chk?>>
								                <label for="<?=$val['pm_cat_slug']?>_add_field<?=$i?>_val"></label>
				                            </div>
										</div>
										<label class="col-sm-2 control-label" for="<?=$val['pm_cat_slug']?>_add_field<?=$i?>"><b>Field <?=$i?></b></label>
								        <div class="col-sm-5">

								        	<input type="text" class="form-control" id="<?=$val['pm_cat_slug']?>_add_field<?=$i?>" name="<?=$val['pm_cat_slug']?>_add_field<?=$i?>" value="<?=$field_data[$j]['profile_field_name']?>">
								        </div>
								        <div class="col-sm-4">
								        	<select class="form-control" name="<?=$val['pm_cat_slug']?>_field_type<?=$i?>" id="<?=$val['pm_cat_slug']?>_field_type<?=$i?>">
								        		<option value="">Select Field <?=$i?> Type</option>
								        		<option value="text" <?php if($field_data[$j]['profile_field_type']=="text"){ echo "selected"; } ?> >Textbox</option>
								        		<option value="date" <?php if($field_data[$j]['profile_field_type']=="date"){ echo "selected"; } ?> >Date</option>
								        	</select>
										</div>
								    </div>
									<?php
										}
									?>
									
								</div>
						        <?php
						        	}
						        	else
						        	{
						        ?>
						        <div class="form-group" id="<?=$val['pm_cat_slug']?>_field_data">
									<input type="hidden" name="<?=$val['pm_cat_slug']?>_no_of_fields" id="<?=$val['pm_cat_slug']?>_no_of_fields" value="1">
									<div class="row" style="margin-bottom: 10px;" id="<?=$val['pm_cat_slug']?>_row1">
										<div class="col-sm-1 text-right">
											<div class="checkbox">
								                <input id="<?=$val['pm_cat_slug']?>_add_field1_val" name="<?=$val['pm_cat_slug']?>_add_field1_val" class="magic-checkbox" type="checkbox" <?php if($val['value'] == "Yes"){ ?>checked<?php } ?>>
								                <label for="<?=$val['pm_cat_slug']?>_add_field1_val"></label>
				                            </div>
										</div>
										<label class="col-sm-2 control-label" for="<?=$val['pm_cat_slug']?>_add_field1"><b>Field 1</b></label>
								        <div class="col-sm-5">

								        	<input type="text" class="form-control" id="<?=$val['pm_cat_slug']?>_add_field1" name="<?=$val['pm_cat_slug']?>_add_field1">
								        </div>
								     	<div class="col-sm-4">
								        	<select class="form-control" name="<?=$val['pm_cat_slug']?>_field_type1" id="<?=$val['pm_cat_slug']?>_field_type1">
								        		<option value="">Select Field <?=$i?> Type</option>
								        		<option value="text" selected>Textbox</option>
								        		<option value="date">Date</option>
								        	</select>
										</div>
								    </div>
								</div>
						        <?php
						        	}
						        ?>
								<div class="form-group">

									<div class="col-sm-offset-2 col-sm-9">

							        	<button type="submit" class="btn btn-primary btn-sm btn-labeled fa fa-save">Save <?=$val['pm_cat_name']?> Fields</button>

									</div>

								</div>
							</form>
					   	</div>
					   	<?php
			                }
	             		?>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<script>

	setTimeout(function() {

	    $('#success_alert').fadeOut('fast');

	    $('#danger_alert').fadeOut('fast');

	}, 5000); // <-- time in milliseconds

	function show_attribute_form(id) {
		$(".attribute_form").hide();
		$("#"+id+"_form").show();
	}

	function field_changes(sect, tp)
	{		
		var old_counter = parseInt($("#"+sect+"_no_of_fields").val());
		
        if(tp=='add')
        {
            var new_counter=old_counter+1;

	        var content='	<div class="row" style="margin-bottom: 10px;" id="'+sect+'_row'+new_counter+'">\
		        				<div class="col-sm-1 text-right">\
									<div class="checkbox">\
						                <input id="'+sect+'_add_field'+new_counter+'_val" name="'+sect+'_add_field'+new_counter+'_val" class="magic-checkbox" type="checkbox" <?php if($val['value'] == "Yes"){ ?>checked<?php } ?>>\
						                <label for="'+sect+'_add_field'+new_counter+'_val"></label>\
				                    </div>\
								</div>\
								<label class="col-sm-2 control-label" for="'+sect+'_add_field'+new_counter+'"><b>Field '+new_counter+'</b></label>\
						        <div class="col-sm-5">\
						        	<input type="text" class="form-control" id="'+sect+'_add_field'+new_counter+'" name="'+sect+'_add_field'+new_counter+'" value="">\
						        </div>\
						        <div class="col-sm-4">\
						        	<select class="form-control" name="'+sect+'_field_type'+new_counter+'" id="'+sect+'_field_type'+new_counter+'">\
						        		<option value="">Select Field '+new_counter+' Type</option>\
						        		<option value="text" selected>Textbox</option>\
						        		<option value="date">Date</option>\
						        	</select>\
								</div>\
					        </div>';
		    $("#"+sect+"_field_data").append(content);
		    $("#"+sect+"_no_of_fields").val(new_counter);
        }
        else
        {
            var new_counter=old_counter-1;
            if(new_counter>0)
            {
            	$("#"+sect+"_row"+old_counter).remove()
            	$("#"+sect+"_no_of_fields").val(new_counter);
            }
        }

        
    }
</script>