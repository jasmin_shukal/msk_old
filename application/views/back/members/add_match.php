
<!--CONTENT CONTAINER-->

<!--===================================================-->

<div id="content-container">

	<div id="page-head">

		<!--Page Title-->

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<div id="page-title">

			<h1 class="page-header text-overflow"><?php echo translate('members')?></h1>

			<!--Searchbox-->

			<div class="searchbox">

				<div class="pull-right">

					<a href="<?=base_url()?>admin/members/<?=$parameter?>" class="btn btn-danger btn-sm btn-labeled fa fa-step-backward" type="submit"><?php echo translate('go_back')?></a>

				</div>

			</div>

		</div>

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<!--End page title-->

		<!--Breadcrumb-->

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<ol class="breadcrumb">

			<li><a href="#"><?php echo translate('home')?></a></li>

			<li><a href="#"><?php echo translate('members')?></a></li>

			<li><a href="#"><?=translate($member_type)?> <?php echo translate('members')?></a></li>

			<li class="active"><?php echo translate('add match')?></li>

		</ol>

		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

		<!--End breadcrumb-->

	</div>

	<!--Page content-->

                <!--===================================================-->

                <div id="page-content">

                    

					<div class="fixed-fluid">

					<?php 

						$members = array();

						if ($member_type == "Free") {

							$member = $get_free_member_by_id;

						}

						elseif ($member_type == "Premium") {

							$member = $get_premium_member_by_id;

						}

					?>

						<?php

							foreach ($member as $value) {

								$image = json_decode($value->profile_image, true);

								$education_and_career = json_decode($value->education_and_career, true);

								$basic_info = json_decode($value->basic_information, true);

								$present_address = json_decode($value->present_address, true);

								$education_and_career = json_decode($value->education_and_career, true);

								$physical_attributes = json_decode($value->physical_attributes, true);

								$language = json_decode($value->language, true);

								$hobbies_and_interest = json_decode($value->hobbies_and_interest, true);

								$personal_attitude_and_behavior = json_decode($value->personal_attitude_and_behavior, true);

								$residency_information = json_decode($value->residency_information, true);

								$spiritual_and_social_background = json_decode($value->spiritual_and_social_background, true);

								$life_style = json_decode($value->life_style, true);

								$astronomic_information = json_decode($value->astronomic_information, true);

								$permanent_address = json_decode($value->permanent_address, true);

								$family_info = json_decode($value->family_info, true);

								$additional_personal_details = json_decode($value->additional_personal_details, true);

								$Partner_expectation = json_decode($value->Partner_expectation, true);

								$package_info = json_decode($value->package_info, true);
                                

						         
								$arr = (array)$package_info[0];	


							    $this->db->select('*');
							    $this->db->from('matching');
							    $this->db->where('main_id',$value->member_id);
							    $already_match = $this->db->get();

							  
							    $check_counter = count($check);

		                        if ($arr['current_package_id']==1)
		                        {
		                        	$check_counter =20 - $already_match->num_rows();
		                        }

		                        elseif ($arr['current_package_id']==2)
		                        {
                                    $check_counter =40 - $already_match->num_rows();
		                        }
		                        else
		                        {
		                        	$check_counter =100 - $already_match->num_rows();
		                        }	
		     

								



							}

							include_once "left_panel.php";

						?>
						<div class="fluid">

	<div class="fixed-fluid">

		<div class="fluid">

			<div class="panel">

				<div class="panel-body">



					<!--Dark Panel-->

			        <!--===================================================-->

			       <!--  <div class="pull-right">

						<button data-target='#delete_modal' data-toggle='modal' class='btn btn-danger btn-sm btn-labeled fa fa-trash' data-toggle='tooltip' data-placement='top' title='".translate('delete_member')."' onclick='delete_member("<?=$value->member_id?>")'><?php echo translate('delete')?></i></button>

						<a href="<?=base_url()?>admin/members/<?=$parameter?>/edit_member/<?=$value->member_id?>" class="btn btn-primary btn-sm btn-labeled fa fa-edit" type="button"><?php echo translate('edit')?></a>

					</div> -->



			        <div class="text-left">

			        	<h4>Member ID - <?=$value->member_profile_id?></h4>

			        </div>

			        <div class="text-right">

			        	<h6>Matching Limit: <?php echo $check_counter; ?></h6>

			        </div>

			        <div class="pull-right">

			        	<form action="<?=base_url()?>admin/send_profile" method="POST">

			        		<input type="hidden" name="selected_profiles" id="selected_profiles" value="">
			        		<input type="hidden" name="main_profile" id="main_profile" value="<?=$value->member_id?>">
			        		<button class="btn btn-success" type="submit">Send Profiles</button>
                        
			        	     
			        	</form>

			        </div>


			        
		        	<table class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th><?php echo translate('Member ID')?></th>
                                <th><?php echo translate('name')?></th>
                                <th data-sortable="false"><?php echo translate('package')?></th>
                                <th><?php echo translate('member_since')?></th>
                                <th><?php echo translate('member_status')?></th>
                                <input type="hidden" id="mkp" value="<?php echo $check_counter; ?>">
                           </tr>
                           <?php
                           	if($already_match->num_rows()>0)
                           	{
	                           	$already_match_data = $already_match->result_array();
	                           	$alm_ids = array();
	                           	foreach ($already_match_data as $alm) {
	                           		array_push($alm_ids, $alm['profile_id']);
	                           	}
                           	}
	                        $this->db->select('*');
	                        $this->db->from('member');
	                        $this->db->where('member_id !=',$value->member_id);
	                        if($already_match->num_rows()>0)
	                        {
	                        	$this->db->where_not_in('member_id',$alm_ids);
	                        }
	                        $members = $this->db->get()->result_array();
							$i=0;
	                        foreach ($members as $mbr) {

	                        	$continue=0;
	                        	$mbr_education_and_career = (array)json_decode($mbr['education_and_career']);
								$mbr_basic_info = (array)json_decode($mbr['basic_information']);
								$mbr_present_address = (array)json_decode($mbr['present_address']);
								$mbr_education_and_career = (array)json_decode($mbr['education_and_career']);
								$mbr_physical_attributes = (array)json_decode($mbr['physical_attributes']);
								$mbr_language = (array)json_decode($mbr['language']);
								$mbr_hobbies_and_interest = (array)json_decode($mbr['hobbies_and_interest']);
								$mbr_personal_attitude_and_behavior = (array)json_decode($mbr['personal_attitude_and_behavior']);
								$mbr_residency_information = (array)json_decode($mbr['residency_information']);
								$mbr_spiritual_and_social_background = (array)json_decode($mbr['spiritual_and_social_background']);
								$mbr_life_style = (array)json_decode($mbr['life_style']);
								$mbr_astronomic_information = (array)json_decode($mbr['astronomic_information']);
								$mbr_permanent_address = (array)json_decode($mbr['permanent_address']);
								$mbr_family_info = (array)json_decode($mbr['family_info']);
								$mbr_additional_personal_details = (array)json_decode($mbr['additional_personal_details']);
								$mbr_Partner_expectation = (array)json_decode($mbr['Partner_expectation']);
								
	                        	if($mbr_basic_info['GENDER']!=$basic_info['GENDER'])
	                        	{
	                        		if($mbr_Partner_expectation['MARITAL STATUS']==$Partner_expectation['MARITAL STATUS'])
		                        	{
		                        		if($mbr_Partner_expectation['MARITAL STATUS']==$Partner_expectation['MARITAL STATUS'])
		                        		{
                                           	if($mbr_Partner_expectation['CASTE \/ SECT']==$Partner_expectation['CASTE \/ SECT'])
                                           	{
                                           	 	if($mbr_Partner_expectation['DRINKING HABITS']==$Partner_expectation['DRINKING HABITS'])
                                           		{
                                                	if($mbr_Partner_expectation['PREFERED STATE']==$Partner_expectation['PREFERED STATE'])
                                                	{
                                                		if($mbr_Partner_expectation['AGE']==$Partner_expectation['AGE'])
                                                		{
	                                                       	if($mbr_Partner_expectation['SUB CASTE']==$Partner_expectation['SUB CASTE'])
	                                                       	{
	                                                       		if($mbr_Partner_expectation['SMOKING HABITS']==$Partner_expectation['SMOKING HABITS'])
	                                                       		{
		                                                       	   	if($mbr_Partner_expectation['MANGLIK']==$Partner_expectation['MANGLIK'])
		                                                        	{
			                                                            if($mbr_Partner_expectation['PREFERED COUNTRY']==$Partner_expectation['PREFERED COUNTRY'])
			                                                            {
			                                                            	if($mbr_Partner_expectation['HEIGHT']==$Partner_expectation['HEIGHT'])
			                                                            	{
			                                                            		if($mbr_Partner_expectation['WEIGHT']==$Partner_expectation['WEIGHT'])
			                                                            		{
			                                                            			if($mbr_Partner_expectation['COUNTRY OF RESIDENCE']==$Partner_expectation['COUNTRY OF RESIDENCE'])
			                                                            			{
			                                                            				if($mbr_Partner_expectation['WITH CHILDREN ACCEPTABLES']==$Partner_expectation['WITH CHILDREN ACCEPTABLES'])
			                                                            				{
			                                                            					if($mbr_Partner_expectation['RELIGION']==$Partner_expectation['RELIGION'])
			                                                            					{
			                                                            						if($mbr_Partner_expectation['EDUCATION']==$Partner_expectation['EDUCATION'])
			                                                            						{
                                                                                                   if($mbr_Partner_expectation['MOTHER TONGUE']==$Partner_expectation['MOTHER TONGUE'])
                                                                                                   {
                                                                                                      $continue=1;
                                                                                                   }
			                                                            						}
			                                                            					}
			                                                            				}
			                                                            			}
			                                                            		}
			                                                            	}
			                                                            }
                                                       	   			}	
                                                       			}
                                                       		}
                                                		}
                                                	}
                                           		}
                                          	}
		                        		}
		                        	}
	                        	}
	                        	
		                        if($continue==1)
		                        {

			                        $obj = json_decode($mbr['package_info']);
			                        $arr = (array)$obj[0]; 
                            ?>

                           	<tr>
                           	    <td><input type="checkbox" class="mit" id="mit<?=$i?>" name="rm" data-id="<?php echo $mbr['member_id']; ?>" onclick="return check_counter(<?=$i?>)"></td>
                           	    <td><?php echo $mbr['member_profile_id']; ?></td>
                           	    <td><?php echo ucwords(strtolower($mbr['first_name']." ".$mbr['last_name'])); ?></td>
                           	    <td><?php echo $arr['current_package']; ?></td>
                           	    <td><?php echo $mbr['member_since']; ?></td>
                           	    <td><?php echo $mbr['status']; ?></td>
                           	</tr>
                          <?php $i++; } } ?>
                          <?php if($i==0)
                          {
                          ?>
                          <tr>
                           	    <th colspan="6" class="text-center">No Data Found</th>
                           	</tr>
                          <?php
                          }
                          ?>
                       </thead>
					</table>


								

							

									

								





								

								

			       <!--  <?php
                        $this->db->select('*');
                        $this->db->from('profile_main_category');
                        $this->db->where('pm_cat_id',15);
                        $this->db->order_by('pm_cat_id','ASC');
                        $profile_section = $this->db->get()->result_array();
                        foreach ($profile_section as $prsect) {
                    ?>

					<div class="panel panel-dark">

		            	<div class="panel-heading"><h3 class="panel-title"><?=$prsect['pm_cat_name']?></h3></div>
			            <div class="panel-body">

			                <table class="table table-condenced">

			                	<?php
				                	
		                		$member = (array)$get_premium_member_by_id[0];
                                $obj = json_decode($member[$prsect['pm_cat_slug']]);
                                $arr = (array)$obj;
		                		
                                $this->db->select('*');
                                $this->db->from('profile_field');
                                $this->db->where('profile_section_id',15);
                                $this->db->where('profile_field_value','Yes');
                                $this->db->order_by('profile_field_id','ASC');
                                $field_data = $this->db->get()->result_array();
                                $i=0;
                                foreach ($field_data as $prfld) {
                                    $i++;
                                    if($i==1){ echo "<tr>"; }
                                 ?>
                                <td class="td-label">
                                    <span><?=$prfld['profile_field_name']?></span>
                                </td>
                                <td><?php if($arr[$prfld['profile_field_name']]!='') { echo $arr[$prfld['profile_field_name']]; } ?></td>
	                            <?php if($i==2){ echo "</tr>"; $i=0; } ?>
	                            <?php } ?>
                                
                            </table>
                       	</div>
                    </div>
			        <?php
			        	}
			        ?> -->


				</div>

			</div>

		</div>

	</div>

</div>


					</div>					

                </div>

                <!--===================================================-->

                <!--End page content-->

</div>



<!--Default Bootstrap Modal-->

<!--===================================================-->

<div class="modal fade" id="package_modal" role="dialog" tabindex="-1" aria-labelledby="package_modal" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <!--Modal header-->

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>

                <h4 class="modal-title"><?php echo translate('package_information')?></h4>

            </div>

           	<!--Modal body-->

            <div class="modal-body" id="package_modal_body">

            	

            </div>

        </div>

    </div>

</div>

<div class="modal fade" id="block_modal" role="dialog" tabindex="-1" aria-labelledby="block_modal" aria-hidden="true">

    <div class="modal-dialog" style="width: 400px;">

        <div class="modal-content">

            <!--Modal header-->

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>

                <h4 class="modal-title"><?php echo translate('confirm_your_action')?></h4>

            </div>

           	<!--Modal body-->

            <div class="modal-body">

            	<p><?php echo translate('are_you_sure_you_want_to')?> "<b id="block_type"></b>" <?php echo translate('this_user?')?>?</p>

            	<div class="text-right">

            		<input type="hidden" id="member_id" name="member_id" value="">

            		<button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close"><?php echo translate('close')?></button>

                	<button class="btn btn-primary btn-sm" id="block_status" value=""><?php echo translate('confirm')?></button>

            	</div>

            </div>

        </div>

    </div>

</div>

<!--===================================================-->

<!--End Default Bootstrap Modal-->

<!--Default Bootstrap Modal-->

<!--===================================================-->
<?php /*
<div class="modal fade" id="upgrade_modal" role="dialog" tabindex="-1" aria-labelledby="upgrade_modal" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <!--Modal header-->

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>

                <h4 class="modal-title"><?php echo translate('upgrade_package')?></h4>

            </div>

           	<!--Modal body-->

            <div class="modal-body">

            	<form class="form-horizontal" id="package_upgrade_form" method="POST" action="<?=base_url()?>admin/members/upgrade_member_package">

					<div class="row">

					    <div class="col-md-10 col-md-offset-1">

					        <div class="upgrade">

					            <h5><?php echo translate('choose_your_package')?></h5>

					            <div class="text-left">

					                <div class="px-2 py-2">

					                	<input type="hidden" id="up_member_id" name="up_member_id" value="">

					                	<input type="hidden" id="member_type" name="member_type" value="<?=$parameter?>">

					                    <?php echo $this->Crud_model->select_html('plan', 'plan', 'name', 'add', 'form-control form-control-sm selectpicker', '', '', '', '');?>

					                </div>

					            </div>

					        </div>

					    </div>

					</div>

	            	<div class="text-center" style="margin-top: 15px;">

		        		<button class="btn btn-success add-tooltip" type="submit"><?=translate('submit')?></button>

		        	</div>

		        </form>         	

            </div>

        </div>

    </div>

</div>
*/?>
<!--===================================================-->

<!--End Default Bootstrap Modal-->
<script>
	function check_counter(i){
    
    var numberOfChecked = $('.mit:checked').length;
   
    var limit =$("#mkp").val();

    if (numberOfChecked>limit) 
    {
    	alert("Maximum Profiles has been selected");
    	return false;

    }
    else
    {
        var id=$("#mit"+i).attr('data-id');
        var selected_profiles=$("#selected_profiles").val();
        if($("#mit"+i).prop("checked") == true)
        {
	        if (selected_profiles=='') {
	        	$("#selected_profiles").val(id);
	        }
	        else
	        {
	        	$("#selected_profiles").val(selected_profiles+','+id);
	        }
       	}
       	else
       	{
   			var new_selected_profiles = selected_profiles.replace(id,'');
   			$("#selected_profiles").val(new_selected_profiles);
       	}
    	return true;
    }

   
    }

</script>












