   <section class="our-package-indian">
            <div class="container">
                <h2 class="text-head2">Our Packages</h2>
                <h3>World Wide</h3>
                <div class="row our-pack-ind-row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 our-package-indian-left">
                        <img src="Content/images/privilege-suite-world-wide.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 our-package-indian-right">
                        <div class="our-package-indian-text">
                            <h4>Privilege Suite</h4>
                            <p>
                                Our signature service designed for members where success and affluence are a given, and the baseline is “nothing but the best” in life. This offering has no time limit and is as bespoke as it gets. It sets the bar very high for maintaining confidentiality and guarding the privacy of our members who tend to be industrialist families, celebrities and extremely successful professionals.
                                <button href="#" class="mobile-readmore" style="">Read more...</button>
                            </p> 
                            <div class="mobile-collapse">
                                <p>
                                    Naturally, given the highly selective parameters, this search tends to be an extremely slow and refined one with absolutely no specified number of profiles to be shared given the nature of what you are looking for - a compatible partner who will become your confidante and soulmate – a rare combination that is unique and difficult to find.
                                </p>
                                <p>The search is personally handled by our top matchmaker and CEO, Anuradha Gupta, who currently spends her time between New York and India, but having lived in Melbourne, London, Los Angeles and India, brings with her a lot of valuable personal experience and an 'east-meets-west' perspective to understand where one is coming from. Her diverse background, along with an MBA and a background in psychology, uniquely equips her in understanding different personalities, something that is so critical in finding the right partner</p>
                                <p>Privilege Suite members are looking for a certain affluence level as a given, along with Anuradha’s direct involvement - the level of expertise that she brings to the personalized search as well as the hand holding that they feel is required given their criteria.</p>
                                <p>There are two membership options within our Signature service:</p>
                                <a href="PrivilegeSuiteWorldWide.html" class="btn click-continue-btn">Click here to continue...</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row our-pack-ind-row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 order-lg-2 order-md-2 our-package-indian-left">
                        <img src="Content/images/premium-membership-world-wide.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 our-package-indian-right">
                        <div class="our-package-indian-text">
                            <h4>Premium Membership</h4>
                            <p>
                                This 2 year membership is designed for the discerning individual who has an appreciation and fondness for the finer things in life. You are well educated with a global perspective and progressive mindset and looking to share life with someone who truly complements you.
                                <button href="#" class="mobile-readmore2" style="">Read more...</button>
                            </p>
                            <div class="mobile-collapse2">
                                <p>
                                    Since the focus is on the quality and the connect, there are no fixed number of profiles that are shared and in most instances premium member details are shared only with other premium members.
                                </p>
                                <p>Within Premium membership we have two service offerings:</p>
                                <a href="PremiumMembershipWorldWide.html" class="btn click-continue-btn">Click here to continue...</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>