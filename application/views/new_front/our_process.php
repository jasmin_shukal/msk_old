     <!-- content -->
<section class="about-our-process">
    <div class="container">
        <div class="row">
            <div class="section--title text-center col-md-12"><h2 class="text-head2 pb-15">Our Process</h2>
                <div class="whyus">
                    <p>So what is our process? At Mujse Shaadi Karoge, our primary focus is YOU.  Our services are customised and personalised based on your requirements and feedback. We don’t operate on ticking boxes, but instead, we get to the psychology and headspace of our client to find resonance. And most importantly, we believe in respecting privacy. All  the information we gather is confidential and classified with zero data sharing. </p>
                </div>
            </div>
        </div>
    </div>          

            <section id="process" class="about-our-process">
              <div class="container">
                  <div class="section--title text-center col-md-12"><h2 class="text-head2 pb-15">Here's how our partnership works: </h2>
<!--                   <h2 class="text-head2 text-center" style="margin:25px 0 35px">OHere's how our partnership works: </h2> -->
                 </div>
                </div>
              </div>
            </section>
          <!-- content -->
          <div class="row box-design combine">
              <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="margin-top: 65px;">
                  <img src="<?=base_url('new_template/images/our-process.jpeg')?>" class="img-fluid" alt="">
              </div>
              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="process-wrapper">
                  <div id="progress-bar-container">
                    <input type="hidden" id="step-counter" value="1">
                    <ul>
                      <li class="step step1 active"></li>
                      <li class="step step2"></li>
                      <li class="step step3"></li>
                      <li class="step step4"></li>
                    </ul>
                    
                    <div id="line">
                      <div id="line-progress"></div>
                    </div>
                  </div>
                  <!-- new -->
                <div id="progress-content-section">
                    <div class="section-content discovery active">
                      <h2>STEP 1: Create An Authentic Profile </h2>
                      <ul class="text-left div-ul">
                        <li>Sign up HERE. Or ask one of your family members to create it on your behalf.</li>
                        <li>Your information will be kept entirely confidential. It can be accessed only by our team members or you.</li>
                        <li>Please try to represent your authentic self. Think of us as your friend when you are sharing personal details. </li>
                        <li>Remember, the key to an ideal match lies in these details!</li>               
                     </ul>
                     <!--  <p>Sign up <a href="#">HERE</a>. Or ask one of your family members to do it on your behalf.<br/>Your information will be kept completely confidential. It can be accessed only by our team members or you.<br/>Please fill out all the fields to the best of your knowledge. Think of MSK as your friend. Let us know you just like your friends know you. Remember, the key lies in the details!</p> -->
                    </div>
                    
                    <div class="section-content strategy">
                      <h2>STEP 2: Know Your Personal Advisor</h2>
                        <ul class="text-left div-ul">
                            <li>The first call you’ll receive will be a verification call from your advisor.</li>
                            <li>This background check is to ensure the safety and authenticity of every profile on our platform. It will also help you break the ice with your advisor. </li>
                            <li>MSK advisor will be responsible for understanding your preferences and expectations for your future partner.</li>
                        </ul>
                     <!--  <p>Receive a call from your personal advisor. The first call is for verifying your profile information and introducing you to your advisor.<br/>This background check is just to ensure your safety and the genuineness of every profile on Mujse Shaadi Karoge.<br/>Your personal advisor will be responsible for understanding your preferences and needs in a life partner.</p> -->
                    </div>
                    
                    <div class="section-content creative">
                      <h2>STEP 3: Package & Profile Selection </h2>
                      <ul class="text-left div-ul">
                        <li>Based on your liking, select  a matrimonial service package with the help of your advisor. You can also check out the details of our packages HERE.</li>
                        <li>MSK advisor will now create a list of shortlisted profiles based on your preferences.</li>
                     </ul>
                     <!--  <p>Pick a matrimonial service package with the help of your personal advisor. You can also check out the details of our packages <a href="#packages">HERE</a>.<br/>Your personal advisor will now create a shortlist of profiles, based on your choice of filters and the preferences you have outlined.</p> -->
                    </div>
                    
                    <div class="section-content production">
                      <h2>STEP 4: Virtual Meet-Up</h2>
                      <ul class="text-left div-ul">
                        <li>If you come across a profile that interests you, please share that information with your advisor.</li>
                        <li>Our team will get in touch with the prospect to assess and subsequently confirm their interest in your profile.</li>
                        <li>If both of you are willing, we will organize a virtual meet-up for you to interact. </li>
                     </ul>
                    </div>
                  </div>
                  <!-- new -->
                <!--<div id="progress-content-section">
                    <div class="section-content discovery active">
                      <h2>Signup & Create Your Profile on Mujse Shaadi Karoge</h2>
                      <p>Sign up <a href="#">HERE</a>. Or ask one of your family members to do it on your behalf.<br/>Your information will be kept completely confidential. It can be accessed only by our team members or you.<br/>Please fill out all the fields to the best of your knowledge. Think of MSK as your friend. Let us know you just like your friends know you. Remember, the key lies in the details!</p>
                    </div>
                    
                    <div class="section-content strategy">
                      <h2>Meet Your Personal Advisor</h2>
                      <p>Receive a call from your personal advisor. The first call is for verifying your profile information and introducing you to your advisor.<br/>This background check is just to ensure your safety and the genuineness of every profile on Mujse Shaadi Karoge.<br/>Your personal advisor will be responsible for understanding your preferences and needs in a life partner.</p>
                    </div>
                    
                    <div class="section-content creative">
                      <h2>Package Selection & Shortlist</h2>
                      <p>Pick a matrimonial service package with the help of your personal advisor. You can also check out the details of our packages <a href="#packages">HERE</a>.<br/>Your personal advisor will now create a shortlist of profiles, based on your choice of filters and the preferences you have outlined.</p>
                    </div>
                    
                    <div class="section-content production">
                      <h2>Virtual Meet Up</h2>
                      <p>If you find a profile you like, inform your personal advisor.<br/>Our team will get in touch with the probable match to confirm their interest in your profile.<br/>If both sides are willing, we organize a virtual meet up for you to interact with each other.</p>
                    </div>
                  </div> -->
                </div>
              </div>
            </div>
        </div>
    </section>

    <section id="team" class="about-our-team">

        <div class="container">

            <h2 class="text-head2 text-center">Our Team</h2>

            <p  class="text-center" style="color: black;font-size: 20px;"><strong class="strong">We are a bunch of enthusiastic professionals determined to use technology to bring a fairy tale love story into your life!</strong></p>

            <div class="row">
                <div class="col-md-2 team-spec"><?/*base_url('new_template/images/team-members/default.png')*/?>
                    <figure>
                        <img src="<?=base_url('new_template/images_team/jagat-patel.jpeg')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Jagat Patel<br/><span>Director</span></h4>
                    <div class="about-profile-text">
                        <b>Makes Game Changing Business Decisions</b><br><br>  Spent the last 20 years in the education industry,helping students get into world-class universities, now using his intense business focus to steer Mujse Shaadi Karoge to success. If you don’t find him in the office, he is probably shopping, but only for the best international brands!
                    </div>
                </div>
                <div class="col-md-2 team-spec">
                    <figure>
                        <img src="<?=base_url('new_template/images_team/hinal-seth.png')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Hinal Shah<br/><span>Director</span></h4>
                    <div class="about-profile-text">
                       <b>Basically She Runs the Show</b><br><br>A trained hospitality and hospital management professional from Switzerland, Hinal moved on to open several successful restaurants in the United States. She further worked with esteemed,companies like Delta Airlines for 5 years before flying solo with her business ventures. Her exceptional social skills and friendly disposition helps her connect with people effortlessly. Hinal also owns a premium resort near Mumbai where she gets ample opportunities to interact with people from across the world.Hinal is a thorough professional with rare business intellect and great network.
                    </div>
                </div>
                <div class="col-md-2 team-spec">
                    <figure>
                        <img src="<?=base_url('new_template/images_team/rohit-sharma.png')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Rohit Sharma<br/><span>VP operations</span></h4>
                    <div class="about-profile-text">
                        <b>Nothing Goes Wrong On His Watch</b><br><br>Rohit has a rich experience in managing significant events. He is also professionally trained to look into details of grooming, operations and logistics. A people person and an extrovert, he is a pro at organising a party that everyone would remember for years to come. A happy go lucky person with a keen eye for details and strong professional ethics, Rohit is a fine multi-tasker. His passion for travelling and his smooth networking skills makes him a successful businessman.
                    </div>
                </div>
                <div class="col-md-2 team-spec">
                    <figure>
                        <img src="<?=base_url('new_template/images_team/smita-madlani.png')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Smita Madlani<br/><span>VP Mumbai</span></h4>
                    <div class="about-profile-text">
                        <b>Creativity is her super power</b><br><br>Smita Madlani is passionate about all things creative. After studying interior design, she followed her love for fashion. She is a specialist in wedding trousseau wear and personal wardrobe styling.She organises trunk-shows around USA every year. Alongside this she has actively participated in the media industry for 10 years as a costume designer for many plays and Gujarati shows.Recently, during the lockdown she reignited her passion for cooking and introduced her brand called Home Food Kitchen. She believes that food brings people together. To Smita, relationships and love have always been highest priority in life. 
                    </div>
                </div>
                <div class="col-md-2 team-spec">
                    <figure>
                        <img src="<?=base_url('new_template/images_team/neeta-patel.png')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Neeta Patel<br/><span>VP United Kingdom</span></h4>
                    <div class="about-profile-text">
                        <b>Rumoured to Have a Hand in the Harry-Meghan Match</b><br><br> Strong at social networking with a wide circle in the United Kingdom.She Can charm her way into your heart with a friendly smile and loves meeting people.Neeta is a Globe-trotter with her heart set in India.
                    </div>
                </div>

                <div class="col-md-2 team-spec">
                    <figure>
                        <img src="<?=base_url('new_template/images_team/urvi-shah.png')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Urvi Shah<br/><span>VP USA</span></h4>
                    <div class="about-profile-text">
                        <b>A Firm believer of someone, somewhere is made for you!</b><br><br> One of the warmest and affectionate people you can ever meet, Urvi has a diverse corporate experience under her belt. She believes that marriage is a connection of souls, and finding the right one is important to live a fulfilling life. Well-traveled, communications expert and adept at forging strong relationships, Urvi is an ideal fit for our team at Mujse Shaadi Karoge. She’s studied Commerce in India and further acquired double BSc in Accountancy and also in ecommerce from DePaul university in Chicago, Illinois.<br> She always has been associated with different business ventures ranging from clothing, designing, jewelry designing and also early childhood development. After being involved in different areas of work she realized that her main passion was working with people. This coupled with her warm and  friendly disposition, as well as her extensive travel experience, makes her an ideal fit for our company.

                    </div>
                </div>
                
                <div class="col-md-2 team-spec"><?/*base_url('new_template/images/team-members/default.png')*/?>
                    <figure>
                        <img src="<?=base_url('new_template/images_team/bindiya-khurana.png')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Bindiya Khurana<br/><span>Grooming Expert</span></h4>
                    <div class="about-profile-text">
                        <b>She will bring out the best version of you</b><br><br> A certified image consultant, creative hairstylist and renowned make-up artist, Bindiya Khurana is also the founder of BridesByBindiya - an exclusive bespoke bridal styling service. She believes in finding styles for her clients that reflect their inner beauty and brings confidence. Whether you need grooming, styling or sophisticated make-up - with Bindiya, you’re in excellent hands. 
                    </div>
                </div>
                
                <div class="col-md-2 team-spec">
                    <figure>
                        <img src="<?=base_url('new_template/images_team/manubhai-bhojani.png')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Manubhai Bhojani<br/><span>Astrologer</span></h4>
                    <div class="about-profile-text">
                        <b>Finds answers that no one else can </b><br><br> Manubhai is a scholar of Indian astrology, his guidance trusted by hundreds of people around the world. Well-versed in computerized astrological services, he understands the modern Indian mind like no one else. An expert in palmistry, planetary readings, Vaastu and various other astrological sciences, Manubhai helps people find happiness in life.
                    </div>
                </div>

                <div class="col-md-2 team-spec">
                    <figure>
                        <img src="<?=base_url('new_template/images_team/surabhi-tiwari.jpeg')?>" class="img-fluid team-img" alt="">
                        <div class="contact">
                            <a href="#" class="tw"></a>
                            <a href="#" class="fb"></a>
                            <a href="#" class="gp"></a>
                            <a href="#" class="ma"></a>
                        </div>
                    </figure>
                    <h4 class="member-name">Surbhi Tiwari<br/><span>PR</span></h4>
                    <div class="about-profile-text">
                        <b>Surbhi has a background in science and marketing.</b><br><br>She has professionally managed PR for known event companies and clubs all over India. A go getter by nature makes her quick and responsive. She is ambitious and a perfectionist. Her successful influencer career gives her the edge. A delight to work with. People can make sure everything is in control in her watch.
                    </div>
                </div>


            </div>





           <!--  <div class="row">

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 about-our-team-left">

                    <img src="Content/images/geeta.png" class="img-fluid" alt="">

                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 about-our-team-right">

                    <h4>GEETA SHETH<span>SVP, Memberships (India)</span></h4>

                    <div class="about-profile-text">

                        Geeta, who is based in Mumbai has an MBA from the US as well as a Diploma in Advertising and Public Relations and has worked with India's top PR Consultancies. <br /><br />

                        Relating to people comes naturally to her and she enjoys the varied interactions she has with people from all walks of life.<br /><br />

                        Geeta has a cross-cultural marriage that she feels gives her both understanding and perspective on clients in similar situations. She prides herself on understanding the modern relationship being a mother to two grown up daughters.  <br /><br />



                        Living in Mumbai, one of the most progressive cities in the country, she thrives in the melting pot of cultures and socio-economic differences while understanding different relationships and the complexities they come with. She enjoys the meaningful and insightful conversations she is able to have with our older member base as well and brings both humour and compassion to her interactions.<br /><br />



                        Geeta loves to travel and enjoys spending time with family and friends.

                    </div>

                </div>

            </div>



            <div class="row">

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 order-lg-2 order-md-2 about-our-team-left">

                    <img src="Content/images/riema.png" class="img-fluid" alt="">

                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 about-our-team-right">

                    <h4>RIEMA MEHRA<span>VP, Memberships (India)</span></h4>

                    <div class="about-profile-text">

                        Riema is based in New Delhi and with a Masters in English Literature has in the past taught the English language at Lucknow University.<br /><br />

                        She has a compassionate, sensitive and gregarious personality and loves meeting and interacting with new people. Being a mother of two, both children being well on their way to professional success, Riema has a great understanding of the young adult and their thought processes. As a parent she also feels she brings an understanding of apprehensions and thoughts a parents feels through the match-making process.<br /><br />

                        Riema relaxes in her kitchen garden, loves to cook and is always happy to try a new recipe on friends and family.

                    </div>



                </div>

            </div>



            <div class="row">

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 about-our-team-left">

                    <img src="Content/images/namita.png" class="img-fluid" alt="">

                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 about-our-team-right">

                    <h4>NAMITA BHARWANI<span>Senior Manager, Client Relations (India)</span></h4>

                    <div class="about-profile-text">

                        Namita Bharwani is based in Mumbai and has a Masters degree in Commerce. A 60's child she feels well adapted to the family needs of the millennium. She combats her family life and career and is equally devoted to making children have a solid foundation at the Montessori where she contributes.<br /><br />

                        Born in New Delhi, completing her education in Pune and being married in Mumbai, has equipped her with an in-depth knowledge of the melting pot that is India. Namita has an in depth understanding of speaking to clients through the match-making process and understanding their needs and requirements as they journey through the process with us.

                    </div>



                </div>

            </div>



            <div class="row">

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 order-lg-2 order-md-2 about-our-team-left">

                    <img src="Content/images/rachna.png" class="img-fluid" alt="">

                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 about-our-team-right">

                    <h4>RACHNA PRASAD<span>SVP, Memberships (Europe)</span></h4>

                    <div class="about-profile-text">

                        Rachna is based in London,has a Bachelors Degree in English Literature and has worked in the Television News Industry in New Delhi, India for 15 years. Her years in the media have given her the incredible skill of putting people at ease during a conversation, which helps her drive personal conversations with members. <br /><br />

                        Having lived in the US and in London over the last many years gives her a perfect understanding of individuals that live in the west but have grown up with Indian values. <br /><br />

                        Rachna lives with her husband and two children and divides her time equally between family and work. She feels that her strong bonds with family and friends are an integral part of her relationship building skills that help her to both hand hold and empathise with people at this interesting juncture in life where finding a partner becomes a priority. <br /><br />



                        Rachna loves travelling, watching films, meeting new people and volunteers with her sons' cricket club, which also means that she watches endless hours of sport, especially Cricket through the English summer!

                    </div>



                </div>

            </div>



            <div class="row">

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 about-our-team-left">

                    <img src="Content/images/nikita.png" class="img-fluid" alt="">

                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 about-our-team-right">

                    <h4>NIKITA GHOGALE<span>VP, Client Relations (North America) & Marketing Head (Global)</span></h4>

                    <div class="about-profile-text">

                        Nikita Ghogale is based in Portland, Oregon, USA and has a background in media management and spent several years honing her skills in Mumbai, India. Having spent most of her career in a client-facing role, she appreciates the nuances of relationships and brings those skills to VFE with great panache. Connecting with people and being able to guide them in their search gives her immense satisfaction but even more joy when those relationships work out.<br /><br />

                        In her spare time Nikita enjoys exploring the Pacific Northwest. She is incredibly proud of her roots and loves introducing her friends to Indian culture and cuisine.<br /><br />



                    </div>



                </div>

            </div> -->

        </div>

    </section>  