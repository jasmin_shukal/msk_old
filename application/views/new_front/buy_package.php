<style>
	.package_bg
	{
		background-color: #ef6d2100;
	    border-radius: 25px;
	    box-shadow: 1px 0px 5px 1px #707070;
	    padding: 20px 50px;
	}
	.card {
	    position: relative;
	    z-index: 10;
	    border: 1px solid rgba(0, 0, 0, 0.07);
	    border-radius: 0.25rem;
	    background: #fff;
	    -webkit-transition: all 0.3s linear;
	    transition: all 0.3s linear;
	}
	.card-image {
	    position: relative;
	}
	.card-image img, .card-image > .view {
	    max-width: 100%;
	    border-radius: 0.25rem 0.25rem 0 0;
	}
	.bg-base-1 {
	    background-color: transparent !important;
	    color: black !important;
	}
	.span-text {
	    font-weight: 500;
	    letter-spacing: 0.1rem;
	    text-transform: uppercase;
	    font-style: normal;
	    text-align: center;
	    font-size: 11px;
	}
</style>
<section class="about-our-process">
    <div class="container">
        <div class="row">
            <div class="section--title text-center col-md-12"><h2 class="text-head2 pb-15">Buy <?=ucwords(strtolower($selected_plan['name']))?> Package</h2></div>
        </div>
    </div>
    <div class="container">
        <div class="row mb-4">
        	<div class="col-sm-8 col-md-4 ml-auto mr-auto package_bg">
                <div class="feature feature--boxed-border feature--bg-2 active mt-4">
                    <div class="icon-block--style-1-v5 text-center">
                        <div class="block-content">
                            <h3 class="price-tag"><sup style="font-size: 36px;"></sup><?=currency($selected_plan['amount'])?></h3>
                            <ul class="pl-0 pr-0 mt-0 text-left">
                                <?php $skuList = explode(PHP_EOL, $selected_plan['description']);
					               foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
					            ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 package_bg mt-4">
            	<?php
                  $stripe_set = $this->db->get_where('business_settings', array('type' => 'stripe_set'))->row()->value;
                  if ($stripe_set=="ok"){ ?>
                          <div class="card mb-3 card-stripe" style="background: transparent;">
                              <a id="select_stripe">
                              	<div class="card-image">
                                  	<img src="<?=base_url()?>template/front/images/stripe.jpg">
                              	</div>
                              	<div class="text-center bg-base-1" style="height: 26px;border-bottom-left-radius: 3px;    border-bottom-right-radius: 3px;">
                                  	<span class="span-text" id="select_stripe_text">Select</span>
                              	</div>
                              </a>
                          </div>
                      </div>
                  <?php } ?>
            </div>
            <!-- <form id="payment_form" method="POST" action="<?=base_url()?>home/process_payment" enctype="multipart/form-data">
				<div class="text-center pt-3">
					<input type="hidden" name="payment_type" id="payment_type" value="">
					<input type="hidden" name="plan_id" value="<?=$selected_plan['plan_id']?>">
					<?php $exchange=exchange('usd'); $stripe_amount=$selected_plan['amount']/$exchange; ?>
					<input type="hidden" name="stripe_amount" id="stripe_amount" value="<?=$stripe_amount?>">
					<button type="button" class="btn btn-base-1 z-depth-2-bottom" id="purchase_button" disabled>
						<?php echo translate( 'confirm_purchase')?>
					</button>
				</div>
			</form> -->
			<script src="https://js.stripe.com/v2/"></script>
			<script>
				/*$(document).ready(function(e) {
                    //<script src="https://js.stripe.com/v2/"><script>
                    //https://checkout.stripe.com/checkout.js
                    var submittedForm = false;
                    var handler = StripeCheckout.configure({
                        key: '<?php echo $this->db->get_where('business_settings' , array('type' => 'stripe_publishable_key'))->row()->value; ?>',
                        image: '<?php echo base_url(); ?>template/front/images/stripe.jpg',
                        token: function(token) {
                            // Use the token to create the charge with a server-side script.
                            // You can access the token ID with `token.id`
                            $('#payment_form').append("<input type='hidden' name='stripeToken' value='" + token.id + "' />");
                            submittedForm = true;
                            $("#payment_loader").show();
                            $("#payment_section").hide();
                            $("#go_back").hide();
                            $("#purchase_button").hide();
                            setTimeout(function(){
                                $('#payment_form').submit();
                            }, 500);
                        }
                    });
                    $('#select_stripe').on('click', function(e) {
                        $("#select_stripe_text").html("<?php echo translate('selected')?>");
                        $(".card-stripe").css("border", "3px solid #24242D");
                        $("#payment_type").val('stripe');
                        $("#purchase_button").prop('disabled', true);
                        // Open Checkout with further options
                        var total = $('#stripe_amount').val();
                        total = total * 100;
                        handler.open({
                            name: '<?=$this->system_title?>',
                            description: 'Pay with Stripe',
                            amount: total,
                            closed: function() {
                                $("#select_stripe_text").html("<?php echo translate('select')?>");
                                $("#purchase_button").prop('disabled', true);
                                if (submittedForm == false) {
                                    $("#payment_loader").hide();
                                    $("#payment_section").show();
                                    $("#go_back").show();
                                }
                            }
                        });
                        e.preventDefault();
                    });
                    // Close Checkout on page navigation
                    $(window).on('popstate', function() {
                        handler.close();
                    });
                });*/
			</script>
        </div>
    </div>
</section>