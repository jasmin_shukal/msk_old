<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?=$title?></title>

        <?php include('inc/meta_css.php') ?>
        <style>
            :root {
              --jess_font: 15px;
            }
        </style>
    </head>

    <body style="position: relative;">
        <div class="se-pre-con"></div>
        <?php include('inc/navbar.php'); ?>
        <?php include($page.'.php'); ?>
        <?php include('inc/footer.php'); ?>
        <div class="float-btn"><a target="_blank" href="https://api.whatsapp.com/send?phone=919510782335"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></div>
    </body>

</html>

<?php include('inc/last_js.php'); ?>







