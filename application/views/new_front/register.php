<style>
  .form-group{
    margin-bottom: 15px;
   }

    .error{
    color: #FF0000 !important;
    display: none;
   }
   .atag{
    color:#6dbab0;
    float:right;
    font-size: 22px;
  }
</style>
<section class="loin-section">

        <div class="container-fluid">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0 login-left hidden-xs"></div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0 login-right">

                    <div class="login-right-form">

                        <h2 class="text-head2">Register</h2>

                      

                        <?php if($this->session->flashdata('alert')!=''){ ?>

                        <div class="alert alert-danger alert-dismissible">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                          <?=$this->session->flashdata('alert')?>

                        </div>

                        <?php } ?>

                        <form action="<?=base_url('home/add_register')?>" class="form-signin box-design" id="frmLogin" method="post">

                            <div class="form-group row">

                                <label for="fname" class="col-lg-3 col-md-4 col-form-label text-md-right">First Name </label>

                                <div class="col-lg-8 col-md-7">

                                    <input autofocus="autofocus" class="form-control text-box single-line" data-val="true" data-val-required="First Name is required" data-validation="required" data-validation-error-msg="Please Enter First Name" id="fname" name="fname" type="text" value="" />
                                    <p class="error" id="err_fname">First Name is required</p>

                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="lname" class="col-lg-3 col-md-4 col-form-label text-md-right">Last Name</label>

                                <div class="col-lg-8 col-md-7">

                                    <input class="form-control text-box single-line" data-val="true" data-val-required="Last Name is required" data-validation="required" data-validation-error-msg="Please Enter Last Name" id="lname" name="lname" type="text" value="" />

                                    <p class="error" id="err_lname">Last Name is required</p>

                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="phoneno" class="col-lg-3 col-md-4 col-form-label text-md-right">Phone No.</label>

                                <div class="col-lg-8 col-md-7">

                                    <input class="form-control text-box single-line" data-val="true" data-val-required="Phone No. is required" data-validation="required" data-validation-error-msg="Please Enter Phone No." id="phoneno" name="phoneno" type="text" value="" />

                                    <p class="error" id="err_phoneno">Phone Number is required</p>

                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="email" class="col-lg-3 col-md-4 col-form-label text-md-right">Email Id </label>

                                <div class="col-lg-8 col-md-7">

                                    <input class="form-control text-box single-line" data-val="true" data-val-required="Email is required" data-validation="email" data-validation-error-msg="Please Enter Valid Email Address" id="email" name="email" type="email" value="" />

                                    <p class="error" id="err_email">Email is required</p>

                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="password" class="col-lg-3 col-md-4 col-form-label text-md-right">Password </label>

                                <div class="col-lg-8 col-md-7">

                                    <input class="form-control text-box single-line password" data-val="true" data-val-required="Password is required" data-validation="required" data-validation-error-msg="Please Enter Correct Password" id="password" name="password" type="password" />

                                    <p class="error" id="err_password">Password is required</p>

                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="password" class="col-lg-3 col-md-4 col-form-label text-md-right">Confirm Password </label>

                                <div class="col-lg-8 col-md-7">

                                    <input class="form-control text-box single-line password" data-val="true" data-val-required="Confirm Password is required" data-validation="required" data-validation-error-msg="Please Enter Correct Confirm Password" id="cpassword" name="password" type="password" />

                                    <p class="error" id="err_cpassword">Confirm Password is required</p>
                                    <p class="error" id="err_message">Password and Confirm Password is not Same!</p>

                                </div>

                            </div>

                            <div style="display: flex; margin-left: 20%;">
                                <input style="width: auto; margin: 15px;" id="terms" name="terms" type="checkbox">
                                <label style="font-size: 18px;" class="col-form-label"  for="terms" >I confirm & accept all <a href="<?=base_url('terms-conditions')?>"target="_blank">Terms And Conditions</a></label>
                            </div>
                            <p class="error" id="err_terms">Please indicate that you accept the Terms and Conditions</p>
                            <div style="display: flex; margin-left: 20%;">
                                <input style="width: auto; margin: 15px;" id="privacy_policy" name="privacy_policy" type="checkbox">
                                <label style="font-size: 18px;"  class="col-form-label" for="privacy_policy" >I confirm & accept all <a href="<?=base_url('privacy-policy')?>" target="_blank">Privacy Policy</a></label>
                            </div>
                            <p class="error" id="err_privacy_policy">Please indicate that you accept the Privacy Policy</p>

                            <div class="form-group row">

                                <div class="col-md-8 offset-md-3 text-center">

                                    <button type="submit" class="btn normal-fonts" onclick="return Register_validation()"> Register </button>

                                </div>

                            </div>

                            <a href="<?=base_url('home/login')?>" class="atag"><b>Already have an account?</b></a>

                            <div style="text-align: center;"><label></label></div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </section>