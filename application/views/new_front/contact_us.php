<style>
  .form-group{
    margin-bottom: 15px;
  }
  .bg-img {
  /* The image used */
  background-image: url("<?=base_url('new_template/images/contact.jpeg')?>");
  width: 100%;
  margin-top: 20px;

  

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}
.overlay {
  background-color: rgba(0, 0, 0, 0.5);
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  position: relative;
  z-index: 1;
}
.contact-address-box, .box-design {
    background: rgba(255, 255, 255, 0.8);
    box-shadow: 1px 0px 5px 1px #ffffff;
    border-radius: 10px;
}
@media screen and (max-width: 768px) {
    .login-right-form{
        width: 90%;
    }

}

</style>

  <section class="about-our-process">

        <div class="container-fluid">

            <h2 class="text-head2 text-center">Contact Us</h2>

        </div>
        <div class="bg-img">
            <div class="overlay">
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0">
                        
                        <div class="login-right-form">
                            <div class="contact-address-box">
                                <div class="address-box-img"> <img style="width: 60px;" src="<?=base_url('new_template/images/map-marker.png')?>" class="img-fluid" alt=""> </div>
                                 <?php foreach ($contact as $value) { ?>
                                <div class="address-box-city-name city-name-click1">
                                    <h2><?=$value['sc_detail']?></h2>
                                </div>
                                <div class="address-box-full-address city-add-open1">
                                    <p><?=$value['address']?><br><br><b>For Enquiry: </b><a href="tel:<?=$value['phone_enq']?>"><?=$value['phone_enq']?></a><br/><b>For Registration: </b><a href="tel:<?=$value['phone_res']?>"><?=$value['phone_res']?></a></p>
                                </div>
                                <?php  } ?>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0 login-right">

                        <div class="login-right-form">

                            <?php if($this->session->flashdata('alert')!=''){ ?>

                            <div class="alert alert-danger alert-dismissible">

                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                              <?=$this->session->flashdata('alert')?>

                            </div>

                            <?php } ?>

                            

                            

                            <form action="#" class="form-signin box-design" id="frmLogin" method="post">

                                <div class="form-group row">

                                    <label class="col-lg-12 col-form-label text-md-center" style="color: #6dbab0">Inquiry For </label>

                                    <div class="col-md-4">

                                        <input type="checkbox" name="inquiry[]" value="astrology">
                                        <label class="col-lg-12 col-form-label text-md-center">Astrology</label>

                                    </div>

                                    <div class="col-md-4">

                                        <input type="checkbox" name="inquiry[]" value="events">
                                        <label class="col-lg-12 col-form-label text-md-center">Events</label>

                                    </div>

                                    <div class="col-md-4">

                                        <input type="checkbox" name="inquiry[]" value="grooming">
                                        <label class="col-lg-12 col-form-label text-md-center">Grooming</label>

                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="fname" class="col-lg-3 col-md-4 col-form-label text-md-right">First Name </label>

                                    <div class="col-lg-8 col-md-7">

                                        <input autofocus="autofocus" class="form-control text-box single-line" id="fname" required="" name="fname" type="text" value="" />

                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="lname" class="col-lg-3 col-md-4 col-form-label text-md-right">Last Name</label>

                                    <div class="col-lg-8 col-md-7">

                                        <input class="form-control text-box single-line" id="lname" name="lname" required="" type="text" value="" />

                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="phoneno" class="col-lg-3 col-md-4 col-form-label text-md-right">Phone No.</label>

                                    <div class="col-lg-8 col-md-7">

                                        <input class="form-control text-box single-line" id="phoneno" name="phoneno" required="" type="text" value="" />

                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="email" class="col-lg-3 col-md-4 col-form-label text-md-right">Email Id </label>

                                    <div class="col-lg-8 col-md-7">

                                        <input class="form-control text-box single-line" id="email" name="email" required="" type="email" value="" />

                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="password" class="col-lg-3 col-md-4 col-form-label text-md-right">Describe in Detail </label>

                                    <div class="col-lg-8 col-md-7">

                                        <textarea class="form-control" rows="8" name="description" required=""></textarea>

                                    </div>

                                </div>

                                <div class="form-group row">

                                    <div class="col-md-8 offset-md-3 text-center">

                                        <button type="submit" class="btn normal-fonts"> Submit </button>

                                    </div>

                                </div>

                            </form>
                         

       
                        </div>

                    </div>

                </div>
            </div>
        </div>

        

    </section>


