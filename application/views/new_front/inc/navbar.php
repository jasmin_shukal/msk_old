<nav class="navbar navbar-expand-md navbar-dark fixed-top vfe-nav">

<div class="container" style="width: 100%">

    <!-- Brand -->

    <a class="navbar-brand" href="<?=base_url()?>">

        <?php

            $header_logo_info = $this->db->get_where('frontend_settings', array('type' => 'header_logo'))->row()->value;

            $header_logo = json_decode($header_logo_info, true);

            if (file_exists('uploads/header_logo/'.$header_logo[0]['image'])) {

        ?>

            <img src="<?=base_url()?>uploads/header_logo/<?=$header_logo[0]['image']?>" alt="">

        <?php

            }

            else {

            ?>

                <img src="<?=base_url()?>uploads/header_logo/default_image.png" alt="">

            <?php

            }

        ?>

    </a>

    <!-- Toggler/collapsibe Button -->

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">

        <span></span>

        <span></span>

        <span></span>

    </button>

    <!-- Navbar links -->

    <div class="collapse navbar-collapse" id="collapsibleNavbar">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"  role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Our Story</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php /*<a class="dropdown-item" href="<?=base_url()?>#story">Our Story</a>*/ ?>
                    <a class="dropdown-item" href="<?=base_url('our-process')?>">Our Process</a>
                    <a class="dropdown-item" href="<?=base_url('our-process')?>#team">Our Team</a>
                    <?php /*<a class="dropdown-item" href="<?=base_url('message-from-founder')?>">Message From The Founder</a>*/ ?>
                </div>
            </li>
            <li class="nav-item dropdown" id="aboutus">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                       <a class="dropdown-item" href="<?=base_url('our-promise')?>">Our Promise</a>
                       <a class="dropdown-item" href="<?=base_url('advantage')?>">Advantage / Why Us</a>
                </div>
            </li>
            <li class="nav-item" id="bespoke-services"> <a class="nav-link" href="<?=base_url('bespoke-services')?>">Bespoke Services</a> </li>
            <li class="nav-item" id="membership"> <a class="nav-link" href="<?=base_url('membership')?>">Membership</a> </li>
            <li class="nav-item" id="faq"> <a class="nav-link" href="<?=base_url('faq')?>">FAQ</a> </li>
            <?php /*<li class="nav-item" id="blogs"> <a class="nav-link" href="<?=base_url()?>">Blogs</a> </li>*/ ?>
            <li class="nav-item" id="contactus"> <a class="nav-link" href="<?=base_url('contact-us')?>">Contact Us</a> </li>
            <?php /*<li class="nav-item active" id="blog"> <a class="nav-link" href="https://vowsforeternity.com/blog">Blog</a> </li>*/ ?>
            <?php
                if(empty($this->session->userdata('member_id')))
                {
            ?>
            <li class="nav-item"><div class="nav-link"><a href="<?=base_url('Login')?>" class="btn-sm btn btn-res navbar-btn">Login</a></div></li>
            <li class="nav-item"><div class="nav-link"><a href="<?=base_url('Register')?>" class="btn-sm btn btn-res navbar-btn">Register</a></div></li>
        <?php
            }
            else
            {
        ?>
            <li class="nav-item"><a href="<?=base_url().'home/profile'?>" class="btn-sm btn navbar-btn">View Profile</a></li>
            <li class="nav-item"><a href="<?=base_url().'home/logout'?>" class="btn-sm btn navbar-btn">Logout</a></li>
        <?php
            }
        ?>
    </div>

</div>

</nav>
<?php if($page!="home" && $page!="login" && $page!="register") { ?>
<img class="sticker" src="<?=base_url('new_template/images/katputli-both.png')?>">
<?php } ?>