<script src="<?=base_url('new_template/js/bootstrap.min.js')?>"></script>

<script src="<?=base_url('new_template/js/intlTelInput.js')?>"></script>

<script>
    var path = window.location.pathname;

    if (path.toLowerCase().indexOf("apply") != -1

        || path.toLowerCase().indexOf("contactus") != -1

        || path.toLowerCase().indexOf("membership") != -1

        || path.toLowerCase().indexOf("profile") != -1) {

        var input = document.querySelector("#phone");

        window.intlTelInput(input, {

            utilsScript: "<?=base_url('new_template/js/utils.js')?>",

           

        });

    }

</script>



<script src="<?=base_url('new_template/js/custom.js')?>"></script>

<script src='//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js'></script>

<script>
  //Banner Animation
    var rect = $('#container')[0].getBoundingClientRect();
    var mouse = {x: 0, y: 0, moved: false};

    $("#container").mousemove(function(e) {
      mouse.moved = true;
      mouse.x = e.clientX - rect.left;
      mouse.y = e.clientY - rect.top;
    });
     
    // Ticker event will be called on every frame
    TweenLite.ticker.addEventListener('tick', function(){
      if (mouse.moved){    
        parallaxIt(".male", -200);
        parallaxIt(".female", -100);
      }
      mouse.moved = false;
    });

    function parallaxIt(target, movement) {
      TweenMax.to(target, 0.5, {
        x: (mouse.x - rect.width / 2) / rect.width * movement,
        y: (mouse.y - rect.height / 2) / rect.height * movement
      });
    }

    $(window).on('resize scroll', function(){
      rect = $('#container')[0].getBoundingClientRect();
    });


  //Package Animation
    /*var rect1 = $('#container1')[0].getBoundingClientRect();
    var mouse1 = {x: 0, y: 0, moved: false};

    $("#container1").mousemove(function(e) {
      mouse1.moved = true;
      mouse1.x = e.clientX - rect1.left;
      mouse1.y = e.clientY - rect1.top;
    });
     
    // Ticker event will be called on every frame
    TweenLite.ticker.addEventListener('tick', function(){
      if (mouse1.moved){    
        parallaxIt1(".bg", -100);
        parallaxIt1(".ear", -90);
        parallaxIt1(".head", -80);
        parallaxIt1(".face", -70);
        parallaxIt1(".trunk", -60);
        parallaxIt1(".bg-img", -30);
      }
      mouse1.moved = false;
    });

    function parallaxIt1(target, movement) {
      TweenMax.to(target, 0.5, {
        x: (mouse1.x - rect1.width / 2) / rect1.width * movement,
        y: (mouse1.y - rect1.height / 2) / rect1.height * movement
      });
    }

    $(window).on('resize scroll', function(){
      rect1 = $('#container1')[0].getBoundingClientRect();
    });*/

    
    $("input,textarea").blur(function(){
      if( $(this).val() ){
        $(this).parent().addClass("filled");
      } else {
        $(this).parent().removeClass("filled");
      }
    });

    $(".enq-contact1").on("click",function(){
      $(".enq-contact-form1").toggleClass("active");
    });
    $(".enq-contact2").on("click",function(){
      $(".enq-contact-form2").toggleClass("active");
    });
    $(".enq-contact3").on("click",function(){
      $(".enq-contact-form3").toggleClass("active");
    });

    $(".enq-contact-form1 input[type=submit], .enq-contact-form1 .close").on("click",function(e){
      e.preventDefault();
      $(".enq-contact-form1").toggleClass("active")
    });
    $(".enq-contact-form2 input[type=submit], .enq-contact-form2 .close").on("click",function(e){
      e.preventDefault();
      $(".enq-contact-form2").toggleClass("active")
    });
    $(".enq-contact-form3 input[type=submit], .enq-contact-form3 .close").on("click",function(e){
      e.preventDefault();
      $(".enq-contact-form3").toggleClass("active")
    });
</script>
<script>
  $( document ).ready(function() {
    setInterval(function(){
      var num = $("#step-counter").val();
      if(num==4) { var new_num = 1; }
      else { var new_num = parseInt(num)+1; }
      $(".step"+new_num).click();
      $("#step-counter").val(new_num);
    }, 3000);
  });
  $(".step").click( function() {
    $(this).addClass("active").prevAll().addClass("active");
    $(this).nextAll().removeClass("active");
  });

  $(".step1").click( function() {
    $("#step-counter").val(1);
    $("#line-progress").css("width", "25%");
    $(".discovery").addClass("active").siblings().removeClass("active");
  });

  $(".step2").click( function() {
    $("#step-counter").val(2);
    $("#line-progress").css("width", "50%");
    $(".strategy").addClass("active").siblings().removeClass("active");
  });

  $(".step3").click( function() {
    $("#step-counter").val(3);
    $("#line-progress").css("width", "75%");
    $(".creative").addClass("active").siblings().removeClass("active");
  });

  $(".step4").click( function() {
    $("#step-counter").val(4);
    $("#line-progress").css("width", "100%");
    $(".production").addClass("active").siblings().removeClass("active");
  });
</script>
<script>
  function login_validation() {
    var email = $("#email").val();
    var password = $("#password").val();
    var err = 0;
    if(email=='')
    {
      err++;
      $("#email").css('border','1px solid #FF0000');
      $("#err_email").show();
    }
    else
    {
      $("#email").css('border','');
      $("#err_email").hide();
    }
    if(password=='')
    {
      err++;
      $("#password").css('border','1px solid #FF0000');
      $("#err_password").show();
    }
    else
    {
      $("#password").css('border','');
      $("#err_password").hide();
    }
    if(err>0) { return false; }
    else { return true; }
  }


  function Register_validation() {
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var phoneno = $("#phoneno").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var cpassword = $("#cpassword").val();
    var terms = $("#terms").val();
    var privacy_policy = $("#privacy_policy").val();
    var err = 0;

    if(fname=='')
    {
      err++;
      $("#fname").css('border','1px solid #FF0000');
      $("#err_fname").show();
    }
    else
    {
      $("#fname").css('border','');
      $("#err_fname").hide();
    }
    if(lname=='')
    {
      err++;
      $("#lname").css('border','1px solid #FF0000');
      $("#err_lname").show();
    }
    else
    {
      $("#lname").css('border','');
      $("#err_lname").hide();
    }
    if(phoneno=='')
    {
      err++;
      $("#phoneno").css('border','1px solid #FF0000');
      $("#err_phoneno").show();
    }
    else
    {
      $("#phoneno").css('border','');
      $("#err_phoneno").hide();
    }
     if(email=='')
    {
      err++;
      $("#email").css('border','1px solid #FF0000');
      $("#err_email").show();
    }
    else
    {
      $("#email").css('border','');
      $("#err_email").hide();
    }
     if(password=='')
    {
      err++;
      $("#password").css('border','1px solid #FF0000');
      $("#err_password").show();
    }
    else
    {
      $("#password").css('border','');
      $("#err_password").hide();
    }
     if(cpassword=='')
    {
      err++;
      $("#cpassword").css('border','1px solid #FF0000');
      $("#err_cpassword").show();
    }
    else
    {
      $("#cpassword").css('border','');
      $("#err_cpassword").hide();

      if (password != cpassword)
      {
        err++;
        $("#message").css('border','1px solid #FF0000');
        $("#err_message").show();
      }
      else
      {
        $("#message").css('border','');
        $("#err_message").hide();
      }
    }

     if(!$("#terms").prop("checked"))
    {
      err++;
      $("#terms").css('border','1px solid #FF0000');
      $("#err_terms").show();
    }
    else
    {
      $("#terms").css('border','');
      $("#err_terms").hide();
    }

    if(!$("#privacy_policy").prop("checked"))
    {
      err++;
      $("#privacy_policy").css('border','1px solid #FF0000');
      $("#err_privacy_policy").show();
    }
    else
    {
      $("#privacy_policy").css('border','');
      $("#err_privacy_policy").hide();
    }
    
     if(err>0) { return false; }
    else { return true; }
  }
</script>