<footer class="main-footer">
    <div class="footer-container">
        <div class="row">
            <?php /*
            <div class="col-md-12 text-center">
                <a class="footer-logo" href="<?=base_url()?>">
                    <?php
                        $footer_logo_info = $this->db->get_where('frontend_settings', array('type' => 'footer_logo'))->row()->value;
                        $footer_logo = json_decode($footer_logo_info, true);
                        if (file_exists('uploads/footer_logo/'.$footer_logo[0]['image'])) {
                    ?>
                        <img src="<?=base_url()?>uploads/footer_logo/<?=$footer_logo[0]['image']?>" class="img-fluid" alt="">
                    <?php
                        }
                        else {
                        ?>
                            <img src="<?=base_url()?>uploads/footer_logo/default_image.png" class="img-fluid" alt="">
                        <?php
                        }
                    ?>
                  </a>
            </div>
            */ ?>
            <?php
            $this->db->select('*');
            $this->db->from('plan');
            $query = $this->db->get();
            $plan = $query->result_array();

            ?>


              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <li>Packages</li>
                      <?php
                      foreach ($plan as $pl) 
                      {
                      ?>
                      <li style="font-size:var(--jess_font);"><a href="<?=base_url('membership')?>"><?php echo $pl['name']; ?></a></li>
                      <?php
                      }
                      ?>
                      <!-- <li><a href="<?=base_url('membership')?>">The Courtship Package</a></li>
                      <li><a href="<?=base_url('membership')?>">The Soul Mate Package</a></li> -->
                </ul>
              </div>
              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <li>Know More</li>
                      <li style="font-size:var(--jess_font);"><a href="<?=base_url('bespoke-services')?>">Our Services</a></li>
                      <li style="font-size:var(--jess_font);"><a href="<?=base_url('our-promise')?>">Our Promise</a></li>
                      <li style="font-size:var(--jess_font);"><a href="<?=base_url('contact-us')?>">Contact Us</a></li>
                  </ul>
              </div>
              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <li>Legal</li>
                      <li style="font-size:var(--jess_font);"><a href="<?=base_url('privacy-policy')?>">Privacy Policy</a></li>
                      <li style="font-size:var(--jess_font);"><a href="<?=base_url('terms-conditions')?>">Terms And Conditions</a></li>
                      
                  </ul>
              </div>
              <div class="col-xs-6 col-sm-3 col-lg-3">
                  <ul>
                      <!-- <li>Get in touch</li>
                      <li><a href="#">Contact support</a></li> -->
                      <li class="div-label">Follow us on social</li>
                      <li class="social">
                            <a href="https://www.facebook.com/Mujse-Shaadi-Karoge-104592608052151/" title="Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/mujse_shaadi_karoge/" title="Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="copyright">
              <p>All Rights Reserved © <?=date('Y')?>. <a href="<?=base_url()?>" class="" target="_blank">Mujse Shaadi Karoge</a>. Designed & Developed by <a href="https://www.anantsoftcomputing.com/" class="" target="_blank">Anant Soft Computing</a></p>
          </div>
      </div>
    </footer>



<link href="<?=base_url('new_template/css/jquery-ui-1.12.css')?>" rel="stylesheet" />

<script src="<?=base_url('new_template/js/jquery-ui-1.12.js')?>"></script>

<link href="<?=base_url('new_template/css/timeout-dialog.css')?>" rel="stylesheet" />

<script src="<?=base_url('new_template/js/timeout-dialog.js')?>"></script>

<script src='//cdn.jsdelivr.net/jquery.slick/1.5.0/slick.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script>
<script>
  /*$('#quotes').slick({
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 800,
    slidesToShow: 1,
    adaptiveHeight: true
  });*/
  $('#quotes').slick({
    draggable: true,
    autoplay: true,
    autoplaySpeed: 7000,
    arrows: false,
    dots: true,
    fade: true,
    speed: 500,
    infinite: true,
    cssEase: 'ease-in-out',
    touchThreshold: 100
  })
  $( document ).ready(function() {
    $('.no-fouc').removeClass('no-fouc');
  });
  $(window).on("load", function() {
    $(".se-pre-con").fadeOut("slow");;
  });
  var swiper = new Swiper('.blog-slider', {
    spaceBetween: 30,
    effect: 'fade',
    loop: true,
    mousewheel: {
      invert: false,
    },
    // autoHeight: true,
    pagination: {
      el: '.blog-slider__pagination',
      clickable: true,
    }
  });
</script>