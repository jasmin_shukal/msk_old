<link href="<?=base_url('new_template/images/favicon.ico')?>" rel="/shortcut icon">

<link href="<?=base_url('new_template/css/bootstrap.min.css')?>" rel="stylesheet"/>

<link href="<?=base_url('new_template/css/font-awesome.min.css')?>" rel="stylesheet"/>

<link href="<?=base_url('new_template/css/intlTelInput.css')?>" rel="stylesheet"/>

<link href="<?=base_url('new_template/css/custom.css')?>" rel="stylesheet"/>

<link href="//fonts.googleapis.com/css2?family=Lora:ital,wght@1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

<link rel='stylesheet' href='//cdn.jsdelivr.net/jquery.slick/1.5.0/slick.css'>
<link rel='stylesheet' href='//cdn.jsdelivr.net/jquery.slick/1.5.0/slick-theme.css'>
<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>

<script src="<?=base_url('new_template/js/jquery-3.4.1.js')?>"></script>