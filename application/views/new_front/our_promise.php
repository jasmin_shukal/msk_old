<section id="process" class="about-our-process">
    <div class="container" style="margin-bottom: 25px;">
        <h2 class="text-head2 text-center">Our Promise</h2>
        <div class="box-design">
            <div class="blog-slider-fix">
                <div class="blog-slider-fix__wrp swiper-wrapper">
                    <div class="blog-slider-fix__item swiper-slide div-swiper">
                        <div class="blog-slider-fix__img">
                            <img src="<?=base_url('new_template/images/our-promise.jpeg')?>" alt="">
                        </div>
                        <div class="blog-slider-fix__content">
                            <!-- <span class="blog-slider-fix__code">26 December 2019</span> -->
                            <div class="blog-slider-fix__text" style="font-size:var(--jess_font);">Mujse Shaadi Karoge is an effort to bring soulmates together. We are a trustworthy medium for people in search of a life partner and we help create love stories around the world!<br/><br/>Each profile on Mujse Shaadi Karoge is scrutinized and verified on various levels to create a database of genuine people seeking love. We promise to help you write your ‘happily ever after’ just the way you dream it!</div>
                            <!-- <div class="blog-slider-fix__title">WE PROMISE TO HELP YOU WRITE YOUR FOREVER JUST THE WAY YOU DREAM IT!</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>