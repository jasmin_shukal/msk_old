        <?php
            $this->db->select('*');
            $this->db->from('general_settings');
            $this->db->where('type','faqs');
            $faq_list = $this->db->get()->row_array();
        ?>

        <section class="about-our-process" style="margin-bottom: 30px;">
            <div class="container our-packages">
                <h2 class="text-head2 text-center">Frequently Asked Questions</h2>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!--[Start Accordion]-->

                        <div class="accordion box-design" id="accordionOurPackages">
                            <?php
                                $card_list = json_decode($faq_list['value']);
                                if(count($card_list)>0)
                                {
                                    $i=0;
                                    foreach ($card_list as $cl) {
                                        $i++;
                            ?>
                            <div class="card">
                                <div class="card-header" id="heading<?=$i?>">
                                    <h3>
                                        <button class="btn normal-fonts collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$i?>" aria-expanded="true" aria-controls="collapse<?=$i?>"><?=$cl->question?></button>
                                    </h3>
                                </div>
                                <div id="collapse<?=$i?>" class="collapse" aria-labelledby="heading<?=$i?>" data-parent="#accordionOurPackages">
                                    <div class="card-body"><?=$cl->answer?></div>
                                </div>
                            </div>
                            <?php
                                    }
                                }
                            ?>
                        </div>

                        <!--[End Accordion]-->

                    </div>

                </div>

            </div>

        </section>