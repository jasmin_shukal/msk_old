<style>
  .form-group{
    margin-bottom: 15px;
  }
  .error{
    color: #FF0000 !important;
    display: none;
  }
  .atag{
    color:#6dbab0;
    float:right;
    font-size: 22px;
  }
</style>
<section class="loin-section">

        <div class="container-fluid">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0 login-left hidden-xs"></div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0 login-right">

                    <div class="login-right-form">

                        <h2 class="text-head2">Login</h2>

                      

                        <?php
                            if($this->session->flashdata('alert')!=''){
                                if($this->session->flashdata('alert')=='register_success')
                                {
                        ?>

                        <div class="alert alert-success alert-dismissible">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                          Congratulations..!! You've Registered Successfully..!!

                        </div>

                        <?php
                                }
                                elseif($this->session->flashdata('alert')=='forgot_email_sent')
                                {
                        ?>

                        <div class="alert alert-success alert-dismissible">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                          Email Sent successfully..!!

                        </div>

                        <?php
                                }
                                else
                                {
                        ?>

                        <div class="alert alert-danger alert-dismissible">

                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                          <?=$this->session->flashdata('alert')?>

                        </div>

                        <?php
                                }
                            }
                        ?>
                        

                        <form action="<?=base_url('home/check_login')?>" class="form-signin box-design" id="frmLogin" method="post">

                            <div class="form-group row">

                                <label for="email_address" class="col-lg-3 col-md-4 col-form-label text-md-right">Email Id </label>

                                <div class="col-lg-8 col-md-7">

                                    <input autofocus="autofocus" class="form-control text-box single-line" id="email" name="email" type="email"/>
                                    <p class="error" id="err_email">Email is required</p>

                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="password" class="col-lg-3 col-md-4 col-form-label text-md-right">Password </label>

                                <div class="col-lg-8 col-md-7">

                                    <input class="form-control text-box single-line password" id="password" name="password" type="password" />
                                    <p class="error" id="err_password">Password is required</p>

                                </div>

                            </div>

                            <div class="form-group row">

                                <div class="col-md-12">

                                    <button type="submit" class="btn normal-fonts btn-res" onclick="return login_validation()"> Submit </button>



                                    <a href="<?=base_url('home/forgot_password')?>" class="btn normal-fonts forgot-password btn-res"> Forgot Password? </a>

                                </div>



                            </div>
                            <a href="<?=base_url('home/Register')?>" class="atag"><b>Don't have an account yet?</b></a>

                            <div style="text-align: center;"><label></label></div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </section>