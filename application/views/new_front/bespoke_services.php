<style>
    .desc, .desc-services{
        background-color: #ef6d2100;
        /* border: 2px solid #000000; */
        padding: 15px;
        margin: 20px 15px;
        border-radius: 10px;
        box-shadow: 1px 0px 5px 1px #707070;
        width: 100%
    }
    .desc-services{
        min-height: 790px;
        position: relative;
    }
    .desc-services ul{
        padding: 0 25px;
    }
    .bottom-fix
    {
        position: absolute;
        bottom: 20px;
        left: 25%;
    }
    .col-lg-6 .bottom-fix{
        left: 38%;
    }
    .mb-10{
          margin-bottom: 20px;
    }
    @media only screen and (max-width: 1024px)
   {
    .desc-services{
        min-height: 620px;
    }
    /*.bottom-fix{
        left: 40%;
    }*/
   }
    @media only screen and (max-width: 768px)
   {
    .desc-services{
        min-height: 370px;
    }
    .bottom-fix{
        left: 40%;
    }
   }
   @media only screen and (max-width: 425px)
   {
    .desc-services{
        min-height: 570px;
    }
    /*.bottom-fix{
        left: 40%;
    }*/
   }
   @media only screen and (max-width: 375px)
   {
    .desc-services{
        min-height: 600px;
    }
    /*.bottom-fix{
        left: 40%;
    }*/
   }
   @media only screen and (max-width: 320px)
   {
    .desc-services{
        min-height: 745px;
    }
    /*.bottom-fix{
        left: 40%;
    }*/
   }
</style>
<section id="process" class="about-our-process">

    <div class="container">
        <h2 class="text-head2 text-center">Bespoke Services</h2>
         <?php foreach ($services as $value) { ?>
        <div class="desc">
            <p style="font-size:var(--jess_font);"><?=$value['main_desc']?></p>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="desc-services">
                    <h3 class="text-center" style="color: #6dbab0"><?=$value['tab_f_name']?></h3>
                    <p style="font-size:var(--jess_font);"><?=$value['astro_desc']?></p>
                    <div class="bottom-fix text-center"><button onclick="change_modal_content('astrology')" type="button" class="btn normal-fonts enq-contact" data-toggle="modal" data-target="#send_enq">Send Inquiry</button></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="desc-services">
                    <h3 class="text-center"style="color: #6dbab0" ><?=$value['tab_s_name']?></h3>
                   <!--  <p><?=nl2br($value['grooming_desc'])?></p> -->
                    <ul class="text-left div-ul groom" style="font-size:var(--jess_font);">
                        <?php $groomList = explode(PHP_EOL, $value['grooming_desc']);
                           foreach ($groomList as $values) { echo "<li>".$values."</li>"; }
                        ?>
                    </ul>
                    
                    <div class="bottom-fix text-center"><button onclick="change_modal_content('grooming')" type="button" class="btn normal-fonts enq-contact" data-toggle="modal" data-target="#send_enq">Send Inquiry</button></div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="desc-services">
                    <h3 class="text-center" style="color: #6dbab0"><?=$value['tab_t_name']?></h3>
                    <p style="font-size:var(--jess_font);"><?=$value['events_desc']?></p>
                    <div class="bottom-fix text-center"><button onclick="change_modal_content('events')" type="button" class="btn normal-fonts enq-contact" data-toggle="modal" data-target="#send_enq">Send Inquiry</button></div>
                </div>
            </div>
             <?php  } ?>
        </div>
    </div>
</section>

<div id="send_enq" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form style="margin-top: 50px">
            <input type="text" name="fname" class="mb-10" id="fname" placeholder="Your First Name"><label for="fname"></label>
            <input type="text" name="lname" class="mb-10" id="lname" placeholder="Your Last Name"><label for="fname"></label>
            <input type="text" name="email" class="mb-10" id="email" placeholder="Email Address"><label for="email"></label>
            <input type="text" name="phoneno" class="mb-10" id="phoneno" placeholder="Phone No."><label for="phoneno"></label>
            <textarea name="" id="description" class="mb-10" spellcheck="false" placeholder="Specify For"></textarea><label for="description"></label>
            <div class="control submit"><input type="submit"></div>
        </form>
      </div>
    </div>

  </div>
</div>

<script>
    function change_modal_content(tp) {
        if(tp=='astrology')
        {
            $("#modal-title").html('Send Enquiry for Astrology');
        }
        if(tp=='grooming')
        {
            $("#modal-title").html('Send Enquiry for Grooming');
        }
        if(tp=='events')
        {
            $("#modal-title").html('Send Enquiry for Events');
        }
    }
    $(function() {
        var maxHeight = 0;
        $(".desc-services").each(function(){
        if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
        });
        $(".desc-services").height(maxHeight+75);
    });
    
</script>