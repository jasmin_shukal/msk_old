<style>
  .form-group{
    margin-bottom: 15px;
  }
</style>
<section class="loin-section">

        <div class="container-fluid">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0 login-left hidden-xs"></div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pad-0 login-right">

                    <div class="login-right-form">

                        <h2 class="text-head2">Forgot Password</h2>

                        <form action="<?=base_url('home/process_forget_password')?>" class="form-signin box-design" id="frmLogin" method="post">

                            <div class="form-group row">

                                <label for="email_address" class="col-lg-3 col-md-4 col-form-label text-md-right">Email Id </label>

                                <div class="col-lg-8 col-md-7">

                                    <input autofocus="autofocus" class="form-control text-box single-line" id="email" name="email" type="email" value="" />

                                </div>

                            </div>

                            <div class="form-group row">

                                <div class="col-md-8 offset-md-3">

                                    <button type="submit" class="btn normal-fonts"> Submit </button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </section>