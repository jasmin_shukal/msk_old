<section class="about-our-process" id="process">
  <div class="row">
    <div class="section--title text-center col-md-12">
      <h2 class="text-head2 pb-15">Our Packages</h2>
    </div>
  </div>
  <div id='div-container'>
    <div>
      <?php $i=0; foreach ($d as $value) { $i++; if(($i%2)==0){ $main_cls="div-row-right"; } else{ $main_cls="div-row-left"; } ?>
      <div class='row div-row <?=$main_cls?>'>
        <div class='div-title'> 
          <?php /* <div class='div-bullet'><img class="div-image" src="<?=base_url('new_template/images/hawan-kund.gif')?>"></div> */ ?>
          <div class="div-data">
            <div class='div-top'><?=$value['name']?></div>
            <div class='div-middle'><?=currency($value['amount'])?></div>
            <div class='div-bottom'>
              <?php
                if(empty($this->session->userdata('member_id'))) { $purchase_link = base_url('Register'); }
                else
                {
                  $purchase_link = base_url("home/plans/subscribe/".$value['plan_id']);
                }
              ?>
              <a class="btn div-buy-btn" href="<?=$purchase_link?>">Buy Now</a>
            </div>
          </div>
        </div>
        <div class='div-info'>
          <ul class="text-left div-ul mkp">
            <?php $skuList = explode(PHP_EOL, $value['description']);
               foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
            ?>
          </ul>
        </div>
      </div>
      <?php } ?>
  </div>
</section>

<? /*
<style>
  #container1{
    margin-bottom: 30px; 
  }
</style>
 <section class="about-our-process" id="process">
      <div class="row">
        <div class="section--title text-center col-md-12">
          <h2 class="text-head2 pb-15">Our Packages</h2>
            <div class="row" id="container1">
              <div class="col-md-4">
              <!-- <img src="<?=base_url('new_template/images/banner_component/whole-bg.png')?>" class="component bg"> -->
              <img src="<?=base_url('new_template/images/banner_component/bg.png')?>" class="component bg">
              <img src="<?=base_url('new_template/images/banner_component/ear.png')?>" class="component ear">
              <img src="<?=base_url('new_template/images/banner_component/head.png')?>" class="component head">
              <img src="<?=base_url('new_template/images/banner_component/face.png')?>" class="component face">
              <img src="<?=base_url('new_template/images/banner_component/trunk.png')?>" class="component trunk">
              </div>
             <!--  <div class="col-md-8" id="package-style1">
                 <?php foreach ($d as $value) { ?>
                <div style="margin: 0 auto;">
                  <input class="toggle" id="toggle1" type="checkbox">
                  <div class="social-share-wrap">
                    <div class="cover">
                      <img class="image" style="width: 115px;" src="<?=base_url('new_template/images/hawan-kund.gif')?>">
                      <div class="title"><h3><b><?=$value['name']?></b></h3></div>
                    </div>
                    <div class="content">
                      <label class="label" for="toggle1"><i class="fa fa-share-alt"></i></label>
                      <div class="text">
                        <p><b>You'll Get</b></p>
                        <ul class="text-left">
                          <?php $skuList = explode(PHP_EOL, $value['description']);
                             foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
                          ?><br/><br/>
                        </ul>
                      </div>
                     
                    </div>
                    <div class="social-share-overlay">
                      <label class="close" for="toggle1">&times;</label>
                      <div class="social-share-links">
                        <a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                      </div>
                    </div>
                  </div>
                </div>
                      <?php  } ?>
              </div> -->
              <div class="col-md-8">
                <div class="accordion box-design" id="accordionOurPackages">
                    <?php $i=0; foreach ($d as $value) { $i++; ?>
                    <div class="card">
                        <div class="card-header" id="heading<?=$i?>">
                            <h3>
                                <button class="btn normal-fonts collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$i?>" aria-expanded="true" aria-controls="collapse<?=$i?>"><?=$value['name']?></button>
                            </h3>
                        </div>
                        <div id="collapse<?=$i?>" class="collapse" aria-labelledby="heading<?=$i?>" data-parent="#accordionOurPackages">
                            <div class="card-body">
                              <p><b>You'll Get</b></p>
                              <ul class="text-left">
                                <?php $skuList = explode(PHP_EOL, $value['description']);
                                   foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
                                ?><br/><br/>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  */?>