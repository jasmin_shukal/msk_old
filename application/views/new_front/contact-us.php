   <div class="contact-page-body">
            <section class="contact-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="contact-address-box">
                                <div class="address-box-img"> <img src="Content/images/ind-flag.png" class="img-fluid" alt=""> </div>
                                <div class="address-box-city-name city-name-click1">
                                    <h2>
                                        New Delhi<br>
                                        India
                                    </h2>
                                </div>
                                <div class="address-box-full-address city-add-open1">
                                    <p> Statesman House, 4<sup>th</sup> Floor, Barakhamba Road <span>New Delhi 110001</span> <a href="tel:+91 11 30446450">+91 11 30446450</a> </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="contact-address-box">
                                <div class="address-box-img"> <img src="Content/images/ind-flag.png" class="img-fluid" alt=""> </div>
                                <div class="address-box-city-name city-name-click2">
                                    <h2>
                                        Mumbai<br>
                                        India
                                    </h2>
                                </div>
                                <div class="address-box-full-address city-add-open2">
                                    <p> Level 8, Vibgyor Towers, GBlock, C62, Bandra Kurla Complex, <span>Mumbai 400098</span> <a href="tel:+91 22 40907397">+91 22 40907397</a> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="contact-address-box">
                                <div class="address-box-img"> <img src="Content/images/us-flag.png" class="img-fluid" alt=""> </div>
                                <div class="address-box-city-name city-name-click3">
                                    <h2>
                                        New York<br />
                                        United States
                                    </h2>
                                </div>
                                <div class="address-box-full-address city-add-open3">
                                    <p> 1330 Avenue of the Americas, Suite 23A <span>New York City, 10019</span> <a href="tel:+1 212 653 0953">+1 212 653 0953</a> </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="contact-address-box">
                                <div class="address-box-img"> <img src="Content/images/uk-flag.png" class="img-fluid" alt=""> </div>
                                <div class="address-box-city-name city-name-click4">
                                    <h2>
                                        London<br />
                                        United Kingdom
                                    </h2>
                                </div>
                                <div class="address-box-full-address city-add-open4">
                                    <p> Level 1, Devonshire House One Mayfair Place Mayfair, <span>London W1J 8AJ</span> <a href="tel:+44 20 7268 4967">+44 20 7268 4967</a> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="about-testimonial">
                <div class="container">
                    <div class="banner-text">
                        <div class="banner-text-inner">
                            <div class="banner-heading"> I don’t know what they are called, the spaces between seconds– but I think of you always in those intervals.” </div>
                            <div class="banner-sub-heading">– Salvador Plascencia</div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="contact-form-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 contact-form-section-left"> <img src="Content/images/contact-left-img.jpg" alt="" class="img-fluid"> </div>
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 contact-form-section-right">
                            <div class="contact-form-right">
                                <h2 class="text-head2">Contact Us</h2>
                                <form action="https://www.vowsforeternity.com/Home/ContactUs" class="" method="post">    <div class="form-group row">
                                        <div class="col-lg-12 col-md-12">
                                            <input class="form-control text-box single-line" data-val="true" data-val-required="The Name field is required." data-validation="custom" data-validation-error-msg="Invalid Name" data-validation-regexp="^[a-z A-Z,.&#39;-]+$" id="name" name="Name" placeholder="Name" type="text" value="" />
                                            <span class="bottom-bar"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12 col-md-12">
                                            <input class="form-control text-box single-line" data-val="true" data-val-required="The Email field is required." data-validation="email" data-validation-error-msg="Invalid email" id="email" name="Email" placeholder="Email Address" type="email" value="" />
                                            <span class="bottom-bar"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12 col-md-12">
                                            <input class="form-control text-box single-line" data-val="true" data-val-required="The PhoneNo field is required." data-validation="custom" data-validation-error-msg="Invalid PhoneNumber" data-validation-regexp="^[-+]{0,1}[\s\./0-9]{10,12}$" id="phone" name="PhoneNo" placeholder="Phone Number" type="text" value="" />
                                            <span class="bottom-bar"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12 col-md-12">
                                            <textarea class="form-control" cols="20" data-val="true" data-val-required="The Message field is required." id="message" name="Message" placeholder="Message" rows="5"></textarea>
                                            <span class="bottom-bar"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="lbl"><img id="imgCaptcha" src="data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAAeAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD2CiisvxHdz2Hh2/urZ/LniiLI2AcH6HivIk7K56NKm6k1Bbt2+81KK5hNH8TvGr/8JbjcAcf2bF/jV+S8uU8VWlj5uYHs5JHXaOWDKAc9e5pX7o3lhVe0JqWjenN09UjYorL8R3c9h4dv7q2fy54oiyNgHB+h4rOt9J8SkxSP4q3IcMyf2dGMj0zmjm1sKnhlOn7SU1FXtrfpbsn3OlorCF/dab4i+x385ls745s5WQDy3HWIkAfVSee3NM07UrzU7i81KFmOlxK0drEqjNyy53PnGcZ4GDRzIbwk1HmurWTv0d+nre/3PodBRXEWmq60kWlapLq9reRajMiGwjgC7A3XawO4lO+ffNdBb3tw/iu9smkzbx2sUiJtHDEsCc9ewoUr6f13LrYGdK+qdk+/R2a1S6/LzNeiiiqOEKKKKACsTxf/AMijqn/XA1t0VMlzRaNaNT2VSNS17NP7jzWF/hrGI3JxIuDn/Setbmr61p+l+LNOvry48q2ksZAr7GbOWUjgAnpXXUUuVnoTzCE5qU1OSs1rO+66e7p+JyWs6/pmueENZOm3Pn+TB+8/dsuM9PvAehq5pvg3QLKa3vbew2XEeHV/OkODj0LYroaKfKr3ZzvGShTdKg3GLbfxb3SVnZK+3bqYPjSCK48I6gJUDbI9657MDwRWhcrLZ6FKumwqJorci3iA43BflGPrir1FFt/MyVdqnGm1dJt+WtunyPMrWTSFuLWfRXvZPExmBuEeNssWb96JARtAGTyMY4+tdfa/8jzqP/XlD/6E9b1FJRt/XkdeIzBVb+69U1q7vVp9loraLpfcKKKKs8wKKKKAP//Z" /></div>
                                            <input class="text-box single-line" id="CaptchaText" name="CaptchaText" placeholder="Enter Captcha" type="text" value="" />
                                            <div style="color:red"></div>
                                            <span class="bottom-bar"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 offset-md-12">
                                            <button type="submit" class="btn btn-primary"> Submit </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>