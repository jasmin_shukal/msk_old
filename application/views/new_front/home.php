    <head>
      <style>
        .mb-10{
          margin-bottom: 20px;
        }
        
      </style>
    </head>

    <section class="banner stroke-design">
      <div class="row" id="container">
        <?php /*<img src="<?=base_url('new_template/images/banner_component/whole-bg.png')?>" class="component bg">
        <img src="<?=base_url('new_template/images/banner_component/bg.png')?>" class="component bg">
        <img src="<?=base_url('new_template/images/banner_component/ear.png')?>" class="component ear">
        <img src="<?=base_url('new_template/images/banner_component/head.png')?>" class="component head">
        <img src="<?=base_url('new_template/images/banner_component/face.png')?>" class="component face">
        <img src="<?=base_url('new_template/images/banner_component/trunk.png')?>" class="component trunk">*/ ?>
        <img src="<?=base_url('new_template/images/banner_component/katputli-male.png')?>" class="component male">
        <img src="<?=base_url('new_template/images/banner_component/katputli-female.png')?>" class="component female">
        <div class="col-md-4"></div>
        <div class="col-md-8">

          <div id="quotes">
             <div class="bubble">
              <p>"Happily ever after begins here...."</p>
            </div>
            <div class="bubble">
              <p>"I vow to love you with all my heart."</p>
            </div>
            <div class="bubble">
              <p>"I vow to find a happy forever after with you."</p>
            </div>
            <div class="bubble">
              <p>"I vow to always be loyal to our marriage."</p>
            </div>
            <div class="bubble">
              <p>"I vow to raise our children with love & kindness."</p>
            </div>
            <div class="bubble">
              <p>"I vow to build with you,<br/>a life that we can be proud of."</p>
            </div>
            <div class="bubble">
              <p>"I vow to forever cherish your love and friendship."</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section-padding">
        <div class="container">
            <h2 class="text-head2 text-center" id="small-header">Our Story</h2>
            <div class="row">
                <div class="col-xl-5 col-md-12 about-us-img">
                    <img src="<?=base_url('new_template/images/opposites-attract-left.png')?>" alt="Our Story">
                	  <br/>
                    <a href="<?=base_url('Register')?>" class="btn-sm btn btn-res navbar-btn cta marry">Register Now!</a>
                </div>
                <div class="col-xl-7 col-md-12">
                  <h2 class="text-head2" id="big-header">Our Story</h2>
                  <div class="our-story-desc">
                    <div class="left-card">
                      <p style="font-size: var(--jess_font);">When two people are destined to be together, no wait is too long, no distance too far and no obstacle unbeatable. That is why, when my friend Shweta opened up to us about her loneliness - it got me thinking. We can help! If it doesn’t work, nothing changes. But if it works, we can change the lives of two beautiful people.</p>
                      <p  style="font-size: var(--jess_font);">I quickly got to work and scanned through my Facebook friend-list. I already had the right guy in mind but just wanted to double check before doing anything. Sure enough, Aditya was still single and when I spoke to him he agreed to give this a try. Cut to the next week, I introduced Shweta and Aditya at a house-warming party and they hit it off instantly!</p>
                    </div>
                    <div class="right-card">
                      <p  style="font-size: var(--jess_font);">A few months later, Aditya and Shweta are engaged to be married and we couldn’t be happier.</p>
                      <p  style="font-size: var(--jess_font);">That’s when we knew. Matchmaking is an art and a craft, both. An algorithm can deduce but can not feel. Matrimony needs a personalized touch, a supporting hand and a patient ear. This is how we conceived the idea of Mujse Shaadi Karoge.</p>
                      <p  style="font-size: var(--jess_font);">The pandemic has brought us all to a sudden halt. But this isolation has hit single people the hardest. While everyone is looking out to date through social media, we wondered why not have a platform that can help you find someone in a secured and digitally-verified way? We focused our research on real individuals and understanding their needs from a life partner.</p>
                    </div>
                    <div class="left-card">
                      <p  style="font-size: var(--jess_font);">What we found was a real gap. There were lots of people looking for the right partner but there was no right source to lead them to it. That’s why at Mujse Shaadi Karoge we focus on offering you a hassle-free, bespoke matrimonial services experience.</p>
                      <p  style="font-size: var(--jess_font);">We believe that for two people to fall in love, they need to be brought together in the right way.</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why Us -->
    	<section class="section-padding">
        <div class="container">
            <h2 class="text-head2 text-center">Why Us</h2>
            <div class="whyus">
                <p>We offer a personalized experience of matrimonial services like no one else! Choose from a wide range of genuine profiles to find a life partner of your dreams.</p>
            </div>
            <div class="text-head2 text-center">
       	    <a href="<?=base_url('advantage')?>" class="btn-sm btn btn-res navbar-btn cta">Ask Us More!</a>
            </div>

        </div>
    </section>
    <!-- end of Why Us -->
    <section class="section-padding">
        <div class="container">
          <h2 class="text-head2 text-center">Our Process</h2>
          <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 65px;">
                  <img src="<?=base_url('new_template/images/our-process.jpeg')?>" class="img-fluid" alt="">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="process-wrapper">
                  <div id="progress-bar-container">
                    <input type="hidden" id="step-counter" value="1">
                    <ul>
                      <li class="step step1 active"></li>
                      <li class="step step2"></li>
                      <li class="step step3"></li>
                      <li class="step step4"></li>
                    </ul>
                    
                    <div id="line">
                      <div id="line-progress"></div>
                    </div>
                  </div>
                  <div id="progress-content-section">
                    <div class="section-content discovery active">
                      <h2>STEP 1: Create An Authentic Profile </h2>
                      <ul class="text-left div-ul" style="font-size: var(--jess_font);">
                        <li>Sign up HERE. Or ask one of your family members to create it on your behalf.</li>
                        <li>Your information will be kept entirely confidential. It can be accessed only by our team members or you.</li>
                        <li>Please try to represent your authentic self. Think of us as your friend when you are sharing personal details. </li>
                        <li>Remember, the key to an ideal match lies in these details!</li>               
                     </ul>
                    </div>
                    <div class="section-content strategy" style="font-size: var(--jess_font);">
                      <h2>STEP 2: Know Your Personal Advisor</h2>
                        <ul class="text-left div-ul">
                            <li>The first call you’ll receive will be a verification call from your advisor.</li>
                            <li>This background check is to ensure the safety and authenticity of every profile on our platform. It will also help you break the ice with your advisor. </li>
                            <li>MSK advisor will be responsible for understanding your preferences and expectations for your future partner.</li>
                        </ul>
                    </div>
                    <div class="section-content creative" style="font-size: var(--jess_font);">
                      <h2>STEP 3: Package & Profile Selection </h2>
                      <ul class="text-left div-ul">
                        <li>Based on your liking, select  a matrimonial service package with the help of your advisor. You can also check out the details of our packages HERE.</li>
                        <li>MSK advisor will now create a list of shortlisted profiles based on your preferences.</li>
                     </ul>
                    </div>
                    <div class="section-content production" style="font-size: var(--jess_font);">
                      <h2>STEP 4: Virtual Meet-Up</h2>
                      <ul class="text-left div-ul">
                        <li>If you come across a profile that interests you, please share that information with your advisor.</li>
                        <li>Our team will get in touch with the prospect to assess and subsequently confirm their interest in your profile.</li>
                        <li>If both of you are willing, we will organize a virtual meet-up for you to interact. </li>
                     </ul>
                    </div>
                  </div>
                  <!-- <div id="progress-content-section">
                    <div class="section-content discovery active">
                      <h2>Sign up & Create Your Profile on Mujse Shaadi Karoge</h2>
                      <p>Sign up <a href="<?=base_url('Register')?>">HERE</a>. Or ask one of your family members to do it on your behalf.<br/>Your information will be kept completely confidential. It can be accessed only by our team members or you.<br/>Please fill out all the fields to the best of your knowledge. Think of MSK as your friend. Let us know you just like your friends know you. Remember, the key lies in the details!</p>
                    </div>
                    
                    <div class="section-content strategy">
                      <h2>Meet Your Personal Advisor</h2>
                      <p>Receive a call from your personal advisor. The first call is for verifying your profile information and introducing you to your advisor.<br/>This background check is just to ensure your safety and the genuineness of every profile on Mujse Shaadi Karoge.<br/>Your personal advisor will be responsible for understanding your preferences and needs in a life partner.</p>
                    </div>
                    
                    <div class="section-content creative">
                      <h2>Package Selection & Shortlist</h2>
                      <p>Pick a matrimonial service package with the help of your personal advisor. You can also check out the details of our packages <a href="#packages">HERE</a>.<br/>Your personal advisor will now create a shortlist of profiles, based on your choice of filters and the preferences you have outlined.</p>
                    </div>
                    
                    <div class="section-content production">
                      <h2>Virtual Meet Up</h2>
                      <p>If you find a profile you like, inform your personal advisor.<br/>Our team will get in touch with the other party to confirm their interest in your profile.<br/>If both sides are willing, we organize a virtual meet up for you to interact with each other.</p>
                    </div>
                  </div> -->
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
    <section class="section-padding" id="packages">
      <div class="row">
        <div class="section--title text-center col-md-12">
          <h2 class="text-head2 pb-15">Our Packages</h2><p><b>Consult with your advisor and select a package that is best suited to your needs.</b></p>
            <div id='div-container'>
              <div>
                <?php $i=0; foreach ($d as $value) { $i++; if(($i%2)==0){ $main_cls="div-row-right"; } else{ $main_cls="div-row-left"; } ?>
                <div class='row div-row <?=$main_cls?>'>
                  <div class='div-title'> 
                    <?php /*<div class='div-bullet'><img class="div-image" src="<?=base_url('new_template/images/hawan-kund.gif')?>"></div>*/?>
                    <div class="div-data">
                      <div class='div-top'><?=$value['name']?></div>
                      <div class='div-middle'><?=currency($value['amount'])?></div>
                      <div class='div-bottom'>
                        <?php
		                if(empty($this->session->userdata('member_id'))) { $purchase_link = base_url('Register'); }
		                else
		                {
		                  $purchase_link = base_url("home/plans/subscribe/".$value['plan_id']);
		                }
		              ?>
              			<a class="btn div-buy-btn" href="<?=$purchase_link?>">Buy Now</a>
                      </div>
                    </div>
                  </div>
                  <div class='div-info'>
                    <ul class="text-left div-ul mkp" style="font-size: var(--jess_font);">
                      <?php $skuList = explode(PHP_EOL, $value['description']);
                         foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
                      ?>
                    </ul>
                  </div>
                </div>
                <?php } ?>
            </div>
            <? /*
            <div class="row" id="container1">
              <div class="col-md-4">
	              <img src="<?=base_url('new_template/images/banner_component/whole-bg.png')?>" class="component bg">
	              <img src="<?=base_url('new_template/images/banner_component/bg.png')?>" class="component bg">
	              <img src="<?=base_url('new_template/images/banner_component/ear.png')?>" class="component ear">
	              <img src="<?=base_url('new_template/images/banner_component/head.png')?>" class="component head">
	              <img src="<?=base_url('new_template/images/banner_component/face.png')?>" class="component face">
	              <img src="<?=base_url('new_template/images/banner_component/trunk.png')?>" class="component trunk">
              </div>
         	    <div class="col-md-8" id="package-style1">
                <?php foreach ($d as $value) { ?>
                <div style="margin: 0 auto;">
                  <input class="toggle" id="toggle1" type="checkbox">
                  <div class="social-share-wrap">
                    <div class="cover">
                      <img class="image" style="width: 115px;" src="<?=base_url('new_template/images/hawan-kund.gif')?>">
                      <div class="title"><h3><b><?=$value['name']?></b></h3></div>
                    </div>
                    <div class="content">
                      <label class="label" for="toggle1"><i class="fa fa-share-alt"></i></label>
                      <div class="text">
                        <p><b>You'll Get</b></p>
                        <ul class="text-left">
                          <?php $skuList = explode(PHP_EOL, $value['description']);
                             foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
                          ?><br/><br/>
                        </ul>
                      </div>
                     
                    </div>
                    <div class="social-share-overlay">
                      <label class="close" for="toggle1">&times;</label>
                      <div class="social-share-links">
                        <a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                      </div>
                    </div>
                  </div>
                </div>
                <?php  } ?>
              </div>
              <div class="col-md-8">
                <div class="accordion box-design" id="accordionOurPackages">
                	<p>Consult with your advisor and select a package that is best suited to your needs.<p>
                            <?php $i=0; foreach ($d as $value) { $i++; ?>
                            <div class="card">
                                <div class="card-header" id="heading<?=$i?>">
                                    <h3>
                                        <button class="btn normal-fonts collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$i?>" aria-expanded="true" aria-controls="collapse<?=$i?>"><?=$value['name']?></button>
                                    </h3>
                                </div>
                                <div id="collapse<?=$i?>" class="collapse" aria-labelledby="heading<?=$i?>" data-parent="#accordionOurPackages">
                                    <div class="card-body">
                                      <p><b>You'll Get</b></p>
                                      <ul class="text-left">
                                        <?php $skuList = explode(PHP_EOL, $value['description']);
                                           foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
                                        ?><br/><br/>
                                      </ul>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <!-- end of button -->
                            <div class="text-head2 cta-right">
       	    				<a href="https://www.mujseshaadikaroge.com/new/membership" class="btn-sm btn btn-res navbar-btn cta">New, Exciting Deals!</a>
            				</div>
                            <!-- button -->
                        </div>
              </div>
            </div>
            */ ?>
        </div>
        <div class="text-head2 text-center">
          <a href="<?=base_url('membership')?>" class="btn-sm btn btn-res navbar-btn cta">New, Exciting Deals!</a>
        </div>
      </div>
    </section>
    <section class="section-padding" id="inquire_now">
      <div class="container">
        <div class="row">
          <div class="section--title text-center col-md-12"><h2 class="text-head2 pb-15">Bespoke Services</h2>
            <div class="whyus">
                <p style="font-size:var(--jess_font);">We offer a personalized experience of matrimonial services like no one else! Choose from a wide range of genuine profiles to find a life partner of your dreams.</p>
                <!-- <div class="text-head2 text-center">
                   <a href="https://www.mujseshaadikaroge.com/new/contact-us" class="btn-sm btn btn-res navbar-btn cta"> Inquire Now!</a>
            </div> -->
            </div>
            
         </div>
          <div class="col-lg-4">
            <div class="enq-cards">
              <div class="enq-contact-form enq-contact-form1">
                <a href="#" class="close"><i class="fa fa-times"></i></a>
                <form style="margin-top: 50px">
                  <input type="text" class="mb-10" name="fname1" id="fname1" placeholder="Your First Name" /><label for="fname1"></label>
                  <input type="text" class="mb-10" name="lname1" id="lname1" placeholder="Your Last Name"/><label for="lname1"></label>
                  <input type="text" class="mb-10" name="email1" id="email1" placeholder="Email Address"/><label for="email1"></label>
                  <input type="text" class="mb-10" name="phoneno1" id="phoneno1" placeholder=" Phone No."/><label for="phoneno1"></label>
                  <textarea name="description1" class="mb-10" id="description1" placeholder="Specify For"></textarea><label for="description1"></label>
                  <div class="control submit"><input type="submit" /></div>
                </form>
              </div>
               <div class="enq-card active" id="overview">
                <div class="enq-card-content">
                  <div class="row">
                    <div class="col">
                     <?php foreach ($services as $value) { ?> 
                      <h2 style="color: #6dbab0;"><?=$value['tab_f_name']?></h2>
                      <p style="font-size:var(--jess_font);"><?=$value['astro_desc']?></p>
                      <div class="parent-enq"><div class="enq-contact enq-contact1">Send Inquiry</div></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="enq-cards">
              <div class="enq-contact-form enq-contact-form2">
                <a href="#" class="close"><i class="fa fa-times"></i></a>
                <form style="margin-top: 50px">
                  <input type="text" class="mb-10" name="fname2" id="fname2" placeholder="Your First Name"/><label for="fname2"></label>
                  <input type="text" class="mb-10" name="lname2" id="lname2" placeholder="Your Last Name"/><label for="fname2"></label>
                  <input type="text" class="mb-10" name="email2" id="email2" placeholder="Email Address"/><label for="email2"></label>
                  <input type="text" class="mb-10" name="phoneno2" id="phoneno2" placeholder=" Phone No."/><label for="phoneno2"></label>
                  <textarea name="description2" class="mb-10" id="description2" placeholder="Specify For"></textarea><label for="description2"></label>
                  <div class="control submit"><input type="submit" /></div>
                </form>
              </div>
               <div class="enq-card active" id="overview">
                <div class="enq-card-content">
                  <div class="row">
                    <div class="col">
                      <h2 style="color: #6dbab0"><?=$value['tab_s_name']?></h2>
                      <!--  <p><?=$value['grooming_desc']?></p> -->

                      <ul class="text-left div-ul groom" style="font-size: var(--jess_font);">
                        <?php $groomList = explode(PHP_EOL, $value['grooming_desc']);
                           foreach ($groomList as $values) { echo "<li style='font-size: var(--jess_font);'>".$values."</li>"; }
                        ?>
                      </ul>

                      <div class="parent-enq"><div class="enq-contact enq-contact2">Send Inquiry</div></div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <div class="col-lg-4">
            <div class="enq-cards">
              <div class="enq-contact-form enq-contact-form3">
                <a href="#" class="close"><i class="fa fa-times"></i></a>
                <form style="margin-top: 50px">
                  <input type="text" class="mb-10" name="fname3" id="fname3" placeholder="Your First Name"/><label for="fname3"></label>
                  <input type="text" class="mb-10" name="lname3" id="lname3" placeholder="Your Last Name"/><label for="fname3"></label>
                  <input type="text" class="mb-10" name="email3" id="email3" placeholder="Email Address"/><label for="email3"></label>
                  <input type="text" class="mb-10" name="phoneno3" id="phoneno3" placeholder=" Phone No."/><label for="phoneno3"></label>
                  <textarea name="description3" class="mb-10" id="description3" placeholder="Specify For"></textarea><label for="description3"></label>
                  <div class="control submit"><input type="submit" /></div>
                </form>
              </div>
               <div class="enq-card active" id="overview">
                <div class="enq-card-content">
                  <div class="row">
                    <div class="col">
                      <h2 style="color: #6dbab0"><?=$value['tab_t_name']?></h2>
                      <p style="font-size:var(--jess_font);"><?=$value['events_desc']?></p>
                      <div class="parent-enq"><div class="enq-contact enq-contact3">Send Inquiry</div></div>
                    </div>
                  </div>
                </div>
              </div>
              <?php  } ?>
            </div>
          </div>
        </div>
        <!-- button -->
        <!-- <div class="text-head2 text-center">
       	    <a href="https://www.mujseshaadikaroge.com/new/contact-us" class="btn-sm btn btn-res navbar-btn cta">Inquiry Now</a>
        </div> -->
        <!-- end -->
      </div>
    </section>
    
    <!-- <section class="section-padding">
        <div class="container">
          <h2 class="text-head2 text-center">Our Process</h2>
          <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 65px;">
                  <img src="<?=base_url('new_template/images/our-process.jpeg')?>" class="img-fluid" alt="">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="process-wrapper">
                  <div id="progress-bar-container">
                    <input type="hidden" id="step-counter" value="1">
                    <ul>
                      <li class="step step1 active"></li>
                      <li class="step step2"></li>
                      <li class="step step3"></li>
                      <li class="step step4"></li>
                    </ul>
                    
                    <div id="line">
                      <div id="line-progress"></div>
                    </div>
                  </div>

                  <div id="progress-content-section">
                    <div class="section-content discovery active">
                      <h2>Sign up & Create Your Profile on Mujse Shaadi Karoge</h2>
                      <p>Sign up <a href="<?=base_url('Register')?>">HERE</a>. Or ask one of your family members to do it on your behalf.<br/>Your information will be kept completely confidential. It can be accessed only by our team members or you.<br/>Please fill out all the fields to the best of your knowledge. Think of MSK as your friend. Let us know you just like your friends know you. Remember, the key lies in the details!</p>
                    </div>
                    
                    <div class="section-content strategy">
                      <h2>Meet Your Personal Advisor</h2>
                      <p>Receive a call from your personal advisor. The first call is for verifying your profile information and introducing you to your advisor.<br/>This background check is just to ensure your safety and the genuineness of every profile on Mujse Shaadi Karoge.<br/>Your personal advisor will be responsible for understanding your preferences and needs in a life partner.</p>
                    </div>
                    
                    <div class="section-content creative">
                      <h2>Package Selection & Shortlist</h2>
                      <p>Pick a matrimonial service package with the help of your personal advisor. You can also check out the details of our packages <a href="#packages">HERE</a>.<br/>Your personal advisor will now create a shortlist of profiles, based on your choice of filters and the preferences you have outlined.</p>
                    </div>
                    
                    <div class="section-content production">
                      <h2>Virtual Meet Up</h2>
                      <p>If you find a profile you like, inform your personal advisor.<br/>Our team will get in touch with the other party to confirm their interest in your profile.<br/>If both sides are willing, we organize a virtual meet up for you to interact with each other.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section> -->
    <!-- <section class="section-padding" id="packages">
      <div class="row">
        <div class="section--title text-center col-md-12">
          <h2 class="text-head2 pb-15">Our Packages</h2>
          	<div class="row" id="container1">
          		<div class="col-md-4">
			        <?php /*<img src="<?=base_url('new_template/images/banner_component/whole-bg.png')?>" class="component bg">*/ ?>
			        <img src="<?=base_url('new_template/images/banner_component/bg.png')?>" class="component bg">
			        <img src="<?=base_url('new_template/images/banner_component/ear.png')?>" class="component ear">
			        <img src="<?=base_url('new_template/images/banner_component/head.png')?>" class="component head">
			        <img src="<?=base_url('new_template/images/banner_component/face.png')?>" class="component face">
			        <img src="<?=base_url('new_template/images/banner_component/trunk.png')?>" class="component trunk">
          		</div>
          		<div class="col-md-8" id="package-style1">
		             <?php foreach ($d as $value) { ?>
                <div style="margin: 0 auto;">
                  <input class="toggle" id="toggle1" type="checkbox">
                  <div class="social-share-wrap">
                    <div class="cover">
                      <?php /* <img class="image" style="width: 115px;" src="<?=base_url('new_template/images/hawan-kund.gif')?>"> */ ?>
                      <div class="title"><h3><b><?=$value['name']?></b></h3></div>
                    </div>
                    <div class="content">
                      <label class="label" for="toggle1"><i class="fa fa-share-alt"></i></label>
                      <div class="text">
                        <p><b>You'll Get</b></p>
                        <ul class="text-left">
                          <?php $skuList = explode(PHP_EOL, $value['description']);
                             foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
                          ?><br/><br/>
                        </ul>
                      </div>
                     
                    </div>
                    <div class="social-share-overlay">
                      <label class="close" for="toggle1">&times;</label>
                      <div class="social-share-links">
                        <a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                      </div>
                    </div>
                  </div>
                </div>
                      <?php  } ?>
              </div>
              <div class="col-md-8" id="package-style2">
                <div class="accordion box-design" id="accordionOurPackages">
                            <?php $i=0; foreach ($d as $value) { $i++; ?>
                            <div class="card">
                                <div class="card-header" id="heading<?=$i?>">
                                    <h3>
                                        <button class="btn normal-fonts collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$i?>" aria-expanded="true" aria-controls="collapse<?=$i?>"><?=$value['name']?></button>
                                    </h3>
                                </div>
                                <div id="collapse<?=$i?>" class="collapse" aria-labelledby="heading<?=$i?>" data-parent="#accordionOurPackages">
                                    <div class="card-body">
                                      <p><b>You'll Get</b></p>
                                      <ul class="text-left">
                                        <?php $skuList = explode(PHP_EOL, $value['description']);
                                           foreach ($skuList as $values) { echo "<li>".$values."</li>"; }
                                        ?><br/><br/>
                                      </ul>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
              </div>
          	</div>
        </div>
      </div>
    </section> -->
   <!-- <section class="section-padding">
        <div class="container">
            <h2 class="text-head2 text-center">Our Story</h2>
            <div class="row">
                <div class="col-xl-5 col-md-12 about-us-img">
                    <img src="<?=base_url('new_template/images/opposites-attract-left.png')?>" class="img-fluid" alt="">
                </div>
                <div class="col-xl-7 col-md-12">
                  <div class="our-story-desc">
                    <div class="left-card">
                      <p>When two people are destined to be together, no wait is too long, no distance too far and no obstacle unbeatable. That is why, when my friend Shweta opened up to us about her loneliness - it got me thinking. We can help! If it doesn’t work, nothing changes. But if it works, we can change the lives of two beautiful people.</p>
                      <p>I quickly got to work and scanned through my Facebook friend-list. I already had the right guy in mind but just wanted to double check before doing anything. Sure enough, Aditya was still single and when I spoke to him he agreed to give this a try. Cut to the next week, I introduced Shweta and Aditya at a house-warming party and they hit it off instantly!</p>
                    </div>
                    <div class="right-card">
                      <p>A few months later, Aditya and Shweta are engaged to be married and we couldn’t be happier.</p>
                      <p>That’s when we knew. Matchmaking is an art and a craft, both. An algorithm can deduce but can not feel. Matrimony needs a personalized touch, a supporting hand and a patient ear. This is how we conceived the idea of Mujse Shaadi Karoge.</p>
                      <p>The pandemic has brought us all to a sudden halt. But this isolation has hit single people the hardest. While everyone is looking out to date through social media, we wondered why not have a platform that can help you find someone in a secured and digitally-verified way? We focused our research on real individuals and understanding their needs from a life partner.</p>
                    </div>
                    <div class="left-card">
                      <p>What we found was a real gap. There were lots of people looking for the right partner but there was no right source to lead them to it. That’s why at Mujse Shaadi Karoge we focus on offering you a hassle-free, bespoke matrimonial services experience.</p>
                      <p>We believe that for two people to fall in love, they need to be brought together in the right way.</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </section> -->
  <section class="section-padding">
      <div class="container">
        <div class="row">
          <div class="section--title text-center col-md-12"><h2 class="text-head2 pb-15">Testimonials</h2>
            <!-- add content -->
            <div class="whyus">
                <p style="font-size:var(--jess_font);">We love to see people in love! Read what our clients have to say about our services and our practice at Mujse Shaadi Karoge.</p>
            </div>
            <div class="text-head2 text-center">
            <a href="#" class="btn-sm btn btn-res navbar-btn cta">Experience the Magic!</a>
            </div>
            <!-- end content --> 
         </div>
        </div>
      </div>
    </section>
    <script>
      $(function() {
        var screen_width = $( window ).width();
        if(screen_width>1440)
        {
          $("#small-header").hide();
          $("#big-header").show();
          $(".about-us-img img").height($(".our-story-desc").height());
          $(".about-us-img img").css('width','auto');
        }
        else
        {
          $("#small-header").show();
          $("#big-header").hide();
          $(".about-us-img img").addClass('img-fluid');
        }
        if(screen_width<=768)
        {
          $("#inquire_now .enq-cards .enq-card-content .col").css('height','auto');
          $("#inquire_now .enq-cards .enq-card-content .col").each(function(){
            if(screen_width<=320)
            {
              var height = $(this).height() + 100;
            }
            else
            {
              var height = $(this).height() + 75;
            }
            $(this).parent().height((height));
            $(this).parent().parent().height((height+25));
            $(this).parent().parent().parent().parent().height((height));
            $(this).find('.parent-enq').removeClass('parent-enq');
          });
          // $(".pricing-features").height(maxHeight);
        }
        else
        {
          $("#inquire_now .enq-cards .enq-card-content .col").css('height','100%');
        }
      });
    </script>
    