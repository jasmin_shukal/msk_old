    <head>
        <style>
            .flip-card {
                background-color: transparent;
                width: 75%;
                height: 400px;
                margin: 0 auto;
                perspective: 1000px;
            }

            .flip-card-inner {
                position: relative;
                width: 100%;
                height: auto;
                text-align: center;
                transition: transform 0.6s;
                transform-style: preserve-3d;
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            }

            .flip-card:hover .flip-card-inner {
                transform: rotateY(180deg);
            }

            .flip-card-front, .flip-card-back {
                position: absolute;
                width: 100%;
                height: 100%;
                -webkit-backface-visibility: hidden;
                backface-visibility: hidden;
            }

            .flip-card-back {
                transform: rotateY(180deg);
            }
            .why-us-title
            {
                background-image: linear-gradient(147deg, #ffffff 0%, #e5d8ac 74%);
                box-shadow: 1px 0px 5px 1px #707070;
                border-radius: 20px;
                height: 350px;
                padding: 11% 0;
            }
            .why-us-title p
            {
                text-align: center;
                padding: 60px 25px;
                font-size: 32px;
            }
            .why-us-desc{
                color: #4e4a67;
                line-height: 35px;
                font-size: 20px;
                background-color: #FFFFFF;
                border-radius: 20px;
                padding: 12% 40px;
                height: 350px;
                box-shadow: 1px 0px 5px 1px #707070;
            }
            @media screen and (max-width: 1024px) {
                .why-us-desc{
                    line-height: 24px;
                    font-size: 16px;
                }
            }
            @media screen and (max-width: 768px) {
                .why-us-desc{
                    line-height: 21px;
                    font-size: 15px
                }
            }
            @media screen and (max-width: 425px) {
                .why-us-title p{
                    font-size: 24px;
                        
                }
                .flip-card{
                    height: -21px;
                }
                .why-us-title {
                    padding: 25% 0;
                }
                .why-us-desc{
                    padding: 15% 40px;
                }
            }
            @media screen and (max-width: 375px) {
                .why-us-desc{
                    line-height: 18px;
                    font-size: 11px;
                    padding: 28% 40px;
                }
            }
            @media screen and (max-width: 320px) {
                .why-us-desc{
                    line-height: 14px;
                    font-size: 11px;
                }
            }

        </style>
    </head>

    <section class="about-our-process">
        <div class="container" style="margin-bottom: 25px;">
            <h2 class="text-head2 text-center">Why Us</h2>
            <div class="row" style="margin: 55px 20px;">
                <div class="col-md-6">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="why-us-title"><p>Extensive Research & Scrutiny</p></div>
                            </div>
                            <div class="flip-card-back">
                                <div class="why-us-desc">Mujse Shaadi Karoge is not another algorithm-driven, automated matchmaking website. We research and scrutinize every profile (including yours!) to ensure genuine members only. We have very strict policies on misconduct by members and zero tolerance for anyone who exhibits offensive behavior on the MSK platform.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="why-us-title"><p>Simplified Matchmaking Process</p></div>
                            </div>
                            <div class="flip-card-back">
                                <div class="why-us-desc">Mujse Shaadi Karoge simplifies the matchmaking process to make it hassle-free for you. Once you set your preferences and state your personal and professional priorities, it is super easy to find your right match on MSK.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="why-us-title"><p>Elite Profiles</p></div>
                            </div>
                            <div class="flip-card-back">
                                <div class="why-us-desc">Due to our extensive scanning and personalized services you can be sure to find only the most premium family backgrounds on Mujse Shaadi Karoge.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="why-us-title"><p>Dedicated Advisor</p></div>
                            </div>
                            <div class="flip-card-back">
                                <div class="why-us-desc">Mujse Shaadi Karoge assigns a dedicated advisor to you from the moment you sign-up. Your advisor is constantly by your side to take you through the process and to help you find your life partner.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        <!-- <div class="col-md-3">
            <div class="blog-slider__img"><p>Elite Profiles</p></div>
        </div>
        <div class="col-md-3">   
            <div class="blog-slider__img"><p>Simplified Matchmaking Process</p></div>
        </div>
        <div class="col-md-3">   
            <div class="blog-slider__img"><p>Dedicated Advisor</p></div>
        </div>  -->   

            <!-- <div class="box-design">
                <div class="our-members-content">
                    <div class="blog-slider">
                        <div class="blog-slider__wrp swiper-wrapper">
                            <div class="blog-slider__item swiper-slide">
                                <div class="blog-slider__img"><p>Extensive Research & Scrutiny</p></div>
                                <div class="blog-slider__content">
                                    <div class="blog-slider__text">Mujse Shaadi Karoge is not another algorithm-driven, automated matchmaking website. We research and scrutinize every profile (including yours!) to ensure genuine members only. We have very strict policies on misconduct by members and zero tolerance for anyone who exhibits offensive behavior on the MSK platform.</div>
                                </div>
                            </div>
                            <div class="blog-slider__item swiper-slide">
                                <div class="blog-slider__img"><p>Elite Profiles</p></div>
                                <div class="blog-slider__content">
                                    <div class="blog-slider__text">Due to our extensive scanning and personalized services you can be sure to find only the most premium family backgrounds on Mujse Shaadi Karoge.</div>
                                </div>
                            </div>
                            <div class="blog-slider__item swiper-slide">
                                <div class="blog-slider__img"><p>Simplified Matchmaking Process</p></div>
                                <div class="blog-slider__content">
                                    <div class="blog-slider__text">Mujse Shaadi Karoge simplifies the matchmaking process to make it hassle-free for you. Once you set your preferences and state your personal and professional priorities, it is super easy to find your right match on MSK.</div>
                                </div>
                            </div>
                            <div class="blog-slider__item swiper-slide">
                                <div class="blog-slider__img"><p>Dedicated Advisor</p></div>
                                <div class="blog-slider__content">
                                    <div class="blog-slider__text">Mujse Shaadi Karoge assigns a dedicated advisor to you from the moment you sign-up. Your advisor is constantly by your side to take you through the process and to help you find your life partner.</div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-slider__pagination"></div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>