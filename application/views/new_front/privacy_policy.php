        <section class="terms-conditions" style="margin-bottom: 15px;">

            <div class="container">

                <div class="row terms-conditions-row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 terms-conditions-full-col">
                        <h2 class="text-head2 text-center">Privacy Policy</h2>
                    </div>

                </div>

                <div class="row terms-conditions-row box-design">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Mujseshaadikaroge.com is a Global Matrimonial site with regard to the information
                           collected from you, we are strongly committed to your privacy.
                           We use secure server (SSL encryption) for Credit Card/ Debit Card transactions to
                           protect credit card/ debit card information of users/ members. Online payments on
                           mujseshaadikaroge.com is 100% Secure. Your credit card/ debit card information is
                           entered using SSL Technology and 128 Bit Encryption on a Secure Server, which is
                           one of the highest levels of security provided by websites/ portals. </p>
                    </div>

                </div>
                <div class="row terms-conditions-row box-design">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <h3>The information you give to use this site:</h3>

                        

                        <p>Mujseshaadikaroge.com defines Public Information as the personal
                           information that can be displayed on the site, as entered by the member/ user, such
                           as gender, age, height, photograph, etc.
                           Private Information is defined as any information that allows others to contact
                           a member/ user other than through mujseshaadikaroge.com. This information is
                           gathered from members/ users who apply for the various services our portal offers.
                           This private information includes, but is not limited to: email address, credit card
                           numbers, debit card numbers, a user- specified password, mailing address and
                           contact number.
                           Also, mujseshaadikaroge.com allows members/ users to submit public and
                           private information on behalf of others like child, sibling, relative, ward and friend/
                           others. If such child/ sibling/ relative/ ward/ friend/ others do not wish this
                           information to be displayed, she/ he can request the removal of such information
                           providing the necessary evidence that the information pertains to her/ him. 
                          </p>

                       
                    </div>

                </div>

                <div class="row terms-conditions-row box-design">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 terms-conditions-row-right">

                        <h3>How mujseshaadikaroge.com uses the information it collects/ tracks?</h3>

                        <p>The public information submitted by the member/ user is intended for matchmaking
                           purposes, to be displayed to other user/ member seeking a match. This public 
                           information does not reveal your contact, private information. This can be updated/
                           modified by the user/ member, by logging into the user/ member account. </p>

                        <p>The private information like email address is extensively use to provide/ assist you
                           in finding an ideal life partner, to do so you will be receiving emails on your e-mail
						   address from mujseshaadikaroge.com. Your email address is not reveled/ displayed
						   to other users/ members by us. Financial information is confidential and secure, and
						   is used to bill the user/ member for services desired by the user/ member.
						   All other private information is confidential with mujseshaadikaroge.com, and is not
						   disclosed to anyone, except, as required by law, to protect member of
						   mujseshaadikaroge.com. This can be updated/ modified/ deleted by the user/
						   member, by logging into the user/ member account. </p>

						     <p>Mujseshaadikaroge.com does not sell, rent, or loan your personal information to any
								individual, organization unless as required by law.
								We use a Secure Server (SSL encryption) for Credit Card/ Debit Card transactions
								to protect the information of our users/ members/ clients. Online payment on
								mujseshaadikaroge.com is 100% secure. Credit Card/ Debit Card information is
								entered using SSL Technology and 128 Bit Encryption on a Secure Server, which is
								one of highest levels of security provided by websites/ portals.</p>

								<h3><strong>If you have any question regarding our privacy policy, please write it to customer
                                   care/ or contact our customer care number.</strong></h3>
                                   <p>Thanks,<br>
                                      Mujseshaadikaroge.com </p>
                    </div>

                </div>

               
            </div>

        </section>
        









