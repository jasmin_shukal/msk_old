<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '\ZapfDingbats',
    'bold' => $fontDir . '\ZapfDingbats',
    'italic' => $fontDir . '\ZapfDingbats',
    'bold_italic' => $fontDir . '\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '\Symbol',
    'bold' => $fontDir . '\Symbol',
    'italic' => $fontDir . '\Symbol',
    'bold_italic' => $fontDir . '\Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '\DejaVuSans-Bold',
    'bold_italic' => $fontDir . '\DejaVuSans-BoldOblique',
    'italic' => $fontDir . '\DejaVuSans-Oblique',
    'normal' => $fontDir . '\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '\DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '\DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '\DejaVuSansMono-Oblique',
    'normal' => $fontDir . '\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '\DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '\DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '\DejaVuSerif-Italic',
    'normal' => $fontDir . '\DejaVuSerif',
  ),
  'montserrat' => array(
    'normal' => $fontDir . '\f86fa52db42b72e62cfa645e44b5208e',
  ),
  'muli' => array(
    'normal' => $fontDir . '\74f968911c719187f7fb528d347b1fd8',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '\3f41d2bb4ae8944b081c208e62ea5176',
  ),
  'quicksand' => array(
    'normal' => $fontDir . '/928a43fb6528bd1166b64aec4b48b4b9',
  ),
  'roboto condensed' => array(
    'normal' => $fontDir . '/205211c2de4576b9f5637ab2bc5857cc',
  ),
  'slabo 27px' => array(
    'normal' => $fontDir . '/e11980773a41d11500662a4a4820fd76',
  ),
  'lg' => array(
    'normal' => $fontDir . '/9dd3e5e6d4063da49ee43364bb4f4198',
  ),
  'ionicons' => array(
    'normal' => $fontDir . '/9114ea7edc295bf0d83e3ac098df1c13',
  ),
  'simple-line-icons' => array(
    'normal' => $fontDir . '/290207b7099509fe9722b5cb4b2fc720',
  ),
  'cristmas' => array(
    'normal' => $fontDir . '/da43d274853f8d5c082b526012b61fab',
  ),
  'clothes' => array(
    'normal' => $fontDir . '/90c5291bfe99f23ede240b42b782150d',
  ),
  'communication-48-x-48' => array(
    'normal' => $fontDir . '/1b7517e60dde22daadf4a6094f71bdc0',
  ),
  'education-48' => array(
    'normal' => $fontDir . '/616cfd263e571973f73c8bda8ec97434',
  ),
  'electronics' => array(
    'normal' => $fontDir . '/53447454076dcc7638f66bf2a4e8aec7',
  ),
  'finance' => array(
    'normal' => $fontDir . '/4217e9eb267472723d470c006494e315',
  ),
  'food-48' => array(
    'normal' => $fontDir . '/cb42cbd4dcc71d9d3c278f9538eefab0',
  ),
  'furniture' => array(
    'normal' => $fontDir . '/815a56a68eae69bc62197feb237437fc',
  ),
  'hotel-restaurant' => array(
    'normal' => $fontDir . '/bca8d9068201d6fa5cd23f8a1ab7e986',
  ),
  'media' => array(
    'normal' => $fontDir . '/a4efc6c5c847943199f4b42f9e933156',
  ),
  'medical-and-health' => array(
    'normal' => $fontDir . '/9644b67e8b1f8ab0b207dd0d383abdd0',
  ),
  'music' => array(
    'normal' => $fontDir . '/0ee225d63a1b12171ce6c501c03f8b70',
  ),
  'real-estate' => array(
    'normal' => $fontDir . '/5b2f56d7d4716fbb08be7019196dff38',
  ),
  'science' => array(
    'normal' => $fontDir . '/ecf9c6d20cfca2f75556c47747447ee7',
  ),
  'sports-48-x-48' => array(
    'normal' => $fontDir . '/efc77f7062cd512560cb3bea5d27765e',
  ),
  'travel' => array(
    'normal' => $fontDir . '/3cfbebb8899538d2738a50bf5b60b70d',
  ),
  'weather' => array(
    'normal' => $fontDir . '/6b358dd21df9acc2d05851d61564c0a8',
  ),
  'transport' => array(
    'normal' => $fontDir . '/2b7f4f7c1532d15eb190391eeb8126e5',
  ),
  'linea-arrows-10' => array(
    'normal' => $fontDir . '/f34ed146296e20a25ccde6e290fe48d4',
  ),
  'linea-basic-10' => array(
    'normal' => $fontDir . '/fabba6918a491759f4a784e6639681b0',
  ),
  'linea-ecommerce-10' => array(
    'normal' => $fontDir . '/45ea08d8ef9def1337248d0673be940e',
  ),
  'linea-software-10' => array(
    'normal' => $fontDir . '/7f02ea3ffc7e5e151f1acce2cfc927f7',
  ),
  'blzee' => array(
    'normal' => $fontDir . '/7645a4c4433baf0d8bfec99004eee5a8',
  ),
  'sue ellen francisco' => array(
    'normal' => $fontDir . '/28ecfd344f32e2d25fdf9377786b8f39',
  ),
) ?>